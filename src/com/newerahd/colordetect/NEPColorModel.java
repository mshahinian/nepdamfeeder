package com.newerahd.colordetect;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

public class NEPColorModel {

	private static int MAXCOLORS = 14; // Max Colors to keep
	private List<String> primaryHexColors = new ArrayList<String>();
	private List<String> secondaryHexColors = new ArrayList<String>();
	private List<String> colorFamilyHexColors = new ArrayList<String>();
	private List<String> colorFamilyNames = new ArrayList<String>();

	public static String[] colorSpectrum = { "FFC6A5","FF9473","FF6342","FF3118","FF0000","D60000","AD0000","840000","630000","FFE7C6","FFCE9C","FFB573","FF9C4A","FF8429","D66321","AD4A18","844D18","632910","FFFFC6","FFFF9C","FFFF6B","FFFF42","FFFF10","D6C610","AD9410","847308","635208","F7FFCE","EFEFAD","E7F784","DEF763","D6EF39","B5BD31","8C9429","6B6B21","524818","C6EF83","ADDE63","94D639","7BC618","639C18","527B10","425A10","314208","CEEFBD","A5DE94","7BC66B","52B552","299C39","218429","186321","184A18","103910","C6E7DE","94D6CE","63BDB5","31ADA5","089494","087B7B","006363","004A4A","003139","C6EFF7","94D6E7","63C6DE","31B5D6","00A5C6","0084A5","006B84","005263","00394A","BDC6DE","949CCE","6373B5","3152A5","083194","082984","08296B","08215A","00184A","C6B5DE","9C7BBD","7B52A5","522994","31007B","29006B","21005A","21004A","180042","DEBDDE","CE84C6","B552AD","9C2994","8C007B","730063","5A0052","4A0042","390031","F7BDDE","E78CC6","DE5AAD","D63194","CE007B","A50063","840052","6B0042","520031","FFFFFF","E0E0E0","BFBFBF","A1A1A1","808080","616161","000000" };

	public NEPColorModel(List<Map.Entry<Integer, int[]>> colorList) {
		DescriptiveStatistics stats = new DescriptiveStatistics();
		for (Map.Entry<Integer, int[]> e : colorList) {
			stats.addValue(e.getValue()[1]);
		}

		double stdDev = stats.getStandardDeviation();
		double mean = stats.getMean();

		/*System.out.println(String.format("STDDEV %.2f", stdDev));
		System.out.println(String.format("MEAN %.2f", mean));
		*/

		int colorsToKeep = (colorList.size() < MAXCOLORS) ? colorList.size() : MAXCOLORS;
		for (int i = 0; i < colorsToKeep; i++) {
			Map.Entry<Integer, int[]> c = colorList.get(i);
			int[] rgbColor = ColorDetector.getRGBArr(c.getKey());
			String hexColor = ColorDetector.rgbToHexString(rgbColor);
			int totalCount = c.getValue()[1];
			if (i < 2) {
				primaryHexColors.add(hexColor);
			} else {
				double z = Math.abs(totalCount - mean) / stdDev;
				//System.out.println(String.format("%.2f",z));
				if (z > 1) {
					primaryHexColors.add(hexColor);
				} else {
					secondaryHexColors.add(hexColor);
				}
			}
		}
		
		/* Translate to reduced spectrum of main colors */
		for (String pColorHex : primaryHexColors) {
			String spectrumHex = mapPrimaryToColorSpectrum(pColorHex);
			String hashHex = spectrumHex;
			if (!colorFamilyHexColors.contains(hashHex)) {
				colorFamilyHexColors.add(hashHex);
			}
			
			String colorName = ColorNamesDictionary.getColorName(spectrumHex);
			if (!colorFamilyNames.contains(colorName)) {
				colorFamilyNames.add(colorName);
			}
			
		}

	}

	private String mapPrimaryToColorSpectrum(String hexColor) {
		String mappedHex = "";
		double minDistance = Double.MAX_VALUE;
		for(String spectrumHex : colorSpectrum) {
			int[] spectrumRGB = ColorDetector.hexToRGB(spectrumHex);
			int[] thisRGB = ColorDetector.hexToRGB(hexColor);
			double distance = ColorDetector.colorDistance(spectrumRGB, thisRGB);
			if (distance < minDistance) {
				minDistance = distance;
				mappedHex = spectrumHex;
			}
		}
		return mappedHex;
	}

	public String getPrimaryHexColors(String delimiter) {
		if (primaryHexColors != null) {
			return StringUtils.join(primaryHexColors, delimiter);
		}
		return "";
	}

	public String getSecondaryHexColors(String delimiter) {
		if (secondaryHexColors != null) {
			return StringUtils.join(secondaryHexColors, delimiter);
		}
		return "";
	}

	public String getColFamilyHexColors(String delimiter) {
		if (colorFamilyHexColors != null) {
			return StringUtils.join(colorFamilyHexColors, delimiter);
		}
		return "";
	}

	public static String[] getColFamilySpectrum() {
		return colorSpectrum;
	}

	public List<String> getPrimaryHexColors() {
		return primaryHexColors;
	}

	public List<String> getSecondaryHexColors() {
		return secondaryHexColors;
	}

	public List<String> getColorFamilyHexColors() {
		return colorFamilyHexColors;
	}
	
	public List<String> getColorFamilyNames() {
		return colorFamilyNames;
	}

}
