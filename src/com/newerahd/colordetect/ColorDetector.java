package com.newerahd.colordetect;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.apache.log4j.Logger;

public class ColorDetector {
	private static Logger logger = Logger.getLogger(ColorDetector.class);
	private NEPColorModel nepcm;
	private String orientation = "";

	/*
	 * Golden RGB values private static int maxColorDistanceRGB1 = 50; private
	 * static int maxColorDistanceRGB2 = 100; private static int
	 * maxColorDistanceRGB3 = 100;
	 */
	/* Golden Values */
	/*
	 * private static int maxColorDistanceHSB1 = 25; private static int
	 * maxColorDistanceHSB2 = 100; private static int maxColorDistanceHSB3 = 60;
	 */
	private static int maxColorDistanceHSB1 = 10;
	private static int maxColorDistanceHSB2 = 90;
	private static int maxColorDistanceHSB3 = 50;

	private static int maxFinalEntries = 60;

	private static enum ComparatorType {
		PAYLOAD, KEY
	}

	public ColorDetector(File imageFile) throws Exception {
		logger.debug("Processing " + imageFile.getAbsolutePath());
		ImageInputStream is = ImageIO.createImageInputStream(imageFile);
		Iterator<ImageReader> iter = ImageIO.getImageReaders(is);
		if (!iter.hasNext()) {
			throw new Exception("Cannot load  file " + imageFile.getAbsolutePath());
		}
		ImageReader imageReader = (ImageReader) iter.next();
		imageReader.setInput(is);

		BufferedImage image = imageReader.read(0);

		Map<Integer, int[]> m = buildPixelMap(image);

		// Map<Integer, int[]> reduced1 = reduceColors(m, maxColorDistanceRGB1,
		// ComparatorType.KEY);

		Map<Integer, int[]> reduced1 = reduceColorsHSB(m, maxColorDistanceHSB1, ComparatorType.KEY, -1);
		List<Map.Entry<Integer, int[]>> list1 = new LinkedList<Map.Entry<Integer, int[]>>(reduced1.entrySet());
		Collections.sort(list1, new HexColorIntTotalCountComparator());

		// Map<Integer, int[]> reduced2 = reduceColors(reduced1,
		// maxColorDistanceRGB2, ComparatorType.PAYLOAD);

		Map<Integer, int[]> reduced2 = reduceColorsHSB(reduced1, maxColorDistanceHSB2, ComparatorType.PAYLOAD, -1);
		List<Map.Entry<Integer, int[]>> list2 = new LinkedList<Map.Entry<Integer, int[]>>(reduced2.entrySet());
		Collections.sort(list2, new HexColorIntTotalCountComparator());

		int entriesToKeep = (list2.size() < maxFinalEntries) ? list2.size() : maxFinalEntries;
		List<Map.Entry<Integer, int[]>> top50List = list2.subList(0, entriesToKeep);
		Map<Integer, int[]> top50Map = listToMap(top50List);

		// Map<Integer, int[]> reduced3 = reduceColors(top50Map,
		// maxColorDistanceRGB3, ComparatorType.KEY);

		Map<Integer, int[]> reduced3 = reduceColors(top50Map, maxColorDistanceHSB3, ComparatorType.KEY, -1);
		List<Map.Entry<Integer, int[]>> list3 = new LinkedList<Map.Entry<Integer, int[]>>(reduced3.entrySet());
		Collections.sort(list3, new HexColorIntTotalCountComparator());

		nepcm = new NEPColorModel(list3);

	}

	public static Map<Integer, int[]> listToMap(List<Map.Entry<Integer, int[]>> list) {
		Map<Integer, int[]> hm = new HashMap<Integer, int[]>();
		for (Map.Entry<Integer, int[]> e : list) {
			hm.put(e.getKey(), e.getValue());
		}
		return hm;
	}

	public static Map<Integer, int[]> reduceColors(Map<Integer, int[]> initialColorMap, int maxColorDist, ComparatorType compType) {
		return reduceColors(initialColorMap, maxColorDist, compType, -1);
	}

	public static Map<Integer, int[]> reduceColors(Map<Integer, int[]> initialColorMap, int maxColorDist,
			ComparatorType compType, int numEntries) {
		List<Map.Entry<Integer, int[]>> list = new LinkedList<Map.Entry<Integer, int[]>>(initialColorMap.entrySet());
		/* sort by color keys */
		switch (compType) {
		case KEY:
			Collections.sort(list, new HexColorIntKeyComparator());
			break;
		case PAYLOAD:
			Collections.sort(list, new HexColorIntTotalCountComparator());
			break;
		default:
			break;
		}

		int previousColor = Integer.MIN_VALUE;
		Map<Integer, int[]> reducedColorMap = new HashMap<Integer, int[]>();
		for (int i = 0; i < list.size(); i++) {
			if (i == numEntries) {
				break;
			}
			Map.Entry<Integer, int[]> mm = (Map.Entry<Integer, int[]>) list.get(i);
			int thisColor = mm.getKey();
			int pixelCount = mm.getValue()[0];
			int[] thisRGB = getRGBArr(thisColor);
			double colorDist = 0;
			if (previousColor == Integer.MIN_VALUE) {
				reducedColorMap.put(thisColor, setColorPayload(pixelCount, pixelCount));
				previousColor = thisColor;
			} else {
				colorDist = colorDistance(thisRGB, getRGBArr(previousColor));
				if (colorDist <= maxColorDist) {
					int[] previousPayload = reducedColorMap.get(previousColor);
					if (previousPayload[0] >= pixelCount) {
						reducedColorMap.put(previousColor, setColorPayload(pixelCount, previousPayload[1] + pixelCount));
					} else {
						reducedColorMap.remove(previousColor);
						reducedColorMap.put(thisColor, setColorPayload(pixelCount, previousPayload[1] + pixelCount));
						previousColor = thisColor;
					}
				} else {
					reducedColorMap.put(thisColor, setColorPayload(pixelCount, pixelCount));
					previousColor = thisColor;
				}
			}

		}

		return reducedColorMap;
	}

	public static Map<Integer, int[]> reduceColorsHSB(Map<Integer, int[]> initialColorMap, int maxColorDist,
			ComparatorType compType, int numEntries) {
		List<Map.Entry<Integer, int[]>> list = new LinkedList<Map.Entry<Integer, int[]>>(initialColorMap.entrySet());
		/* sort by color keys */
		switch (compType) {
		case KEY:
			Collections.sort(list, new HexColorIntKeyComparator());
			break;
		case PAYLOAD:
			Collections.sort(list, new HexColorIntTotalCountComparator());
			break;
		default:
			break;
		}

		int previousColor = Integer.MIN_VALUE;
		Map<Integer, int[]> reducedColorMap = new HashMap<Integer, int[]>();
		for (int i = 0; i < list.size(); i++) {
			if (i == numEntries) {
				break;
			}
			Map.Entry<Integer, int[]> mm = (Map.Entry<Integer, int[]>) list.get(i);
			int thisColor = mm.getKey();
			int pixelCount = mm.getValue()[0];
			float[] thisHSB = getHSBArr(thisColor);
			double colorDist = 0;
			if (previousColor == Integer.MIN_VALUE) {
				reducedColorMap.put(thisColor, setColorPayload(pixelCount, pixelCount));
				previousColor = thisColor;
			} else {
				colorDist = hsbColorDistance(thisHSB, getHSBArr(previousColor));
				// System.out.println("Max: " + maxColorDist + " " + colorDist
				// );
				if (colorDist <= maxColorDist) {
					int[] previousPayload = reducedColorMap.get(previousColor);
					if (previousPayload[0] >= pixelCount) {
						reducedColorMap.put(previousColor, setColorPayload(pixelCount, previousPayload[1] + pixelCount));
					} else {
						reducedColorMap.remove(previousColor);
						reducedColorMap.put(thisColor, setColorPayload(pixelCount, previousPayload[1] + pixelCount));
						previousColor = thisColor;
					}
				} else {
					reducedColorMap.put(thisColor, setColorPayload(pixelCount, pixelCount));
					previousColor = thisColor;
				}
			}

		}

		return reducedColorMap;
	}

	/**
	 * Return an int array with individual pixel count as well as running total
	 * count
	 * 
	 * @param pixelCount
	 * @param runningCount
	 * @return
	 */
	public static int[] setColorPayload(int pixelCount, int runningCount) {
		int[] pl = new int[2];
		pl[0] = pixelCount;
		pl[1] = runningCount;
		return pl;
	}

	/**
	 * Convert RGB triple to color hex
	 * 
	 * @param rgb
	 * @return string representing color hex
	 */
	public static String rgbToHexString(int[] rgb) {
		return String.format("%02x%02x%02x", rgb[0], rgb[1], rgb[2]);
	}

	public static int[] hexToRGB(String hexColor) {
		if (hexColor.indexOf('#') == 0) {
			hexColor = hexColor.substring(1);
		}
		int[] rgb = new int[3];
		rgb[0] = Integer.valueOf(hexColor.substring(0, 2), 16);
		rgb[1] = Integer.valueOf(hexColor.substring(2, 4), 16);
		rgb[2] = Integer.valueOf(hexColor.substring(4, 6), 16);
		return rgb;
	}

	public static double colorDistance(int[] rgb1, int[] rgb2) {
		double rmean = (rgb1[0] + rgb2[0]) / 2;
		int r = rgb1[0] - rgb2[0];
		int g = rgb1[1] - rgb2[1];
		int b = rgb1[2] - rgb2[2];
		double weightR = 2 + rmean / 256;
		double weightG = 4;
		double weightB = 2 + (255 - rmean) / 256;
		return Math.sqrt(weightR * r * r + weightG * g * g + weightB * b * b);
	}

	public static int[] getRGBArr(int pixel) {
		// int alpha = (pixel >> 24) & 0xff;
		int red = (pixel >> 16) & 0xff;
		int green = (pixel >> 8) & 0xff;
		int blue = (pixel) & 0xff;
		return new int[] { red, green, blue };

	}

	public static double hsbColorDistance(float[] hsv1, float[] hsv2) {
		float A = hsv1[1] * hsv1[0];
		float B = hsv2[1] * hsv2[2];
		float dTheta = hsv2[0] - hsv1[0];
		float dZ = hsv1[2] - hsv2[2];
		double distance = Math.sqrt(dZ * dZ + A * A + B * B + 2 * A * B * Math.cos(dTheta));
		return distance * 100;
	}

	public static float[] getHSBArr(int pixel) {
		int red = (pixel >> 16) & 0xff;
		int green = (pixel >> 8) & 0xff;
		int blue = (pixel) & 0xff;
		float[] hsbvals = new float[3];
		Color.RGBtoHSB(red, green, blue, hsbvals);
		return hsbvals;
	}

	public static boolean isGray(int[] rgbArr) {

		int rgDiff = rgbArr[0] - rgbArr[1];
		int rbDiff = rgbArr[0] - rgbArr[2];
		// Filter out black, white and grays...... (tolerance within 10 pixels)
		int tolerance = 2;
		if (rgDiff > tolerance || rgDiff < -tolerance)
			if (rbDiff > tolerance || rbDiff < -tolerance) {
				return false;
			}
		return true;
	}

	/**
	 * Build hash table with colors and counts
	 * 
	 * @param image
	 * @return Map<Integer, Integer>
	 */
	private Map<Integer, int[]> buildPixelMap(BufferedImage image) {
		int height = image.getHeight();
		int width = image.getWidth();
		this.orientation = computeOrientation(height, width);

		Map<Integer, int[]> m = new HashMap<Integer, int[]>();

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				int rgb = image.getRGB(i, j);
				// int[] rgbArr = getRGBArr(rgb);
				int[] payLoad = new int[2];
				// Filter out grays....
				// if (!isGray(rgbArr)) {
				payLoad = m.get(rgb);
				if (payLoad == null) {
					payLoad = new int[2];
					payLoad[0] = 1;
					payLoad[1] = 0;
				} else {
					payLoad[0]++;
				}
				m.put(rgb, payLoad);
				// }
			}
		}
		return m;
	}

	private String computeOrientation(int height, int width) {
		return (height == width) ? "square" : (height > width) ? "vertical" : "horizontal";
	}

	/**
	 * Comparator for comparing hex color based on color key
	 * 
	 * @author mshahinian
	 * 
	 */
	static class HexColorIntKeyComparator implements Comparator<Map.Entry<Integer, int[]>> {
		public int compare(Map.Entry<Integer, int[]> o1, Map.Entry<Integer, int[]> o2) {
			return ((Comparable<Integer>) o1.getKey()).compareTo(o2.getKey());
		}
	}

	static class HexColorIntTotalCountComparator implements Comparator<Map.Entry<Integer, int[]>> {
		public int compare(Map.Entry<Integer, int[]> o1, Map.Entry<Integer, int[]> o2) {
			int[] o1val = o1.getValue();
			int[] o2val = o2.getValue();
			return (o1val[1] > o2val[1] ? -1 : (o1val[1] == o2val[1] ? 0 : 1));
		}
	}

	/**
	 * Gets Image orientation Square/Horizontal/Vertical
	 * 
	 * @return
	 */
	public String getImageOrientation() {
		return orientation;
	}

	public List<String> getPrimaryHexColors() {
		return nepcm.getPrimaryHexColors();
	}

	public List<String> getSecondaryHexColors() {
		return nepcm.getSecondaryHexColors();
	}

	public List<String> getColorFamilyHexColors() {
		return nepcm.getColorFamilyHexColors();
	}

	public List<String> getColorFamilyNames() {
		return nepcm.getColorFamilyNames();
	}

}
