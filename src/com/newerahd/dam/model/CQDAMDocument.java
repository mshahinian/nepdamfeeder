package com.newerahd.dam.model;

import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;

import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;

import org.apache.commons.lang.StringUtils;

public class CQDAMDocument {

	private String name;
	private Node _parentNode;
	private Node _metaNode;
	private Node _assetNode;
	private Node _contentNode;

	/**
	 * Default Constructor
	 * 
	 * @param parentNode
	 *            - parent Node to add this Document under
	 * @throws Exception
	 */
	public CQDAMDocument(Node parentNode, String nodeName) throws Exception {
		if (parentNode != null) {
			this._parentNode = parentNode;
		} else {
			throw new Exception("Parent Node can't be null");
		}

		if (StringUtils.isEmpty(nodeName)) {
			throw new Exception("You must specify nodeName");
		} else {
			this.name = nodeName;
		}

		// Create Asset node
		this._assetNode = _parentNode.addNode(this.name, "dam:Asset");
		// fileNode.addMixin("mix:referenceable");

		// Create content node under the asset
		this._contentNode = this._assetNode.addNode("jcr:content",
				"dam:AssetContent");
		this._contentNode.setProperty("jcr:lastModified",
				Calendar.getInstance());

		// Create metadata node for the asset content
		this._metaNode = this._contentNode.addNode("metadata",
				"nt:unstructured");
		this.setDefaultMetadata();

	}

	private void setDefaultMetadata() throws ValueFormatException,
			VersionException, LockException, ConstraintViolationException,
			RepositoryException {
		this._metaNode.setProperty("dam:Bitsperpixel", 24);
		this._metaNode.setProperty("dam:Numberofimages", 1);
		this._metaNode.setProperty("dam:Numberoftextualcomments", 0);
		this._metaNode.setProperty("dam:Physicalheightindpi", -1);
		this._metaNode.setProperty("dam:Physicalheightininches", -1);
		this._metaNode.setProperty("dam:Physicalwidthindpi", -1);
		this._metaNode.setProperty("dam:Physicalwidthininches", -1);
		this._metaNode.setProperty("dam:Progressive", "no");
		this._metaNode.setProperty("dam:extracted", Calendar.getInstance()); //
		this._metaNode.setProperty("dc:modified", Calendar.getInstance());
		this._metaNode.setProperty("jcr:lastModified", Calendar.getInstance());
		this._metaNode.addMixin("cq:Taggable");
		this._metaNode.setProperty("tiff:ImageLength", -1);
		this._metaNode.setProperty("tiff:ImageWidth", -1);
	}

	public void setFileFormat(String fileFormat) throws Exception {
		this.checkAddStringProperty("dam:Fileformat", fileFormat);
	}

	public void setMimeType(String mimeType) throws Exception {
		this.checkAddStringProperty("dam:MIMEtype", mimeType);
		this.checkAddStringProperty("dc:format", mimeType);
	}

	public void setAssetAuthor(String author) throws Exception {
		this.checkAddStringProperty("dc:creator", author);
	}

	public void setLastModifiedBy(String userName) throws Exception {
		this.checkAddStringProperty("jcr:lastModifiedBy", userName);
	}

	public void setAssetTitle(String title) throws Exception {
		this.checkAddStringProperty("dc:title", title);
	}

	public void setAssetDescription(String desc) throws Exception {
		this.checkAddStringProperty("dc:description", desc);
	}

	public void setAssetRights(String rights) throws Exception {
		this.checkAddStringProperty("dc:rights", rights);
	}

	public void addMetadata(String key, String value) throws Exception {
		this.checkAddStringProperty(key, value);
	}

	public void addMetadata(String key, String... values) throws Exception {
		if (values != null && values.length > 0) {
			this._metaNode.setProperty(key, values);
		}
	}

	
	public void addMetadata(String key, Calendar value) throws Exception {
		this._metaNode.setProperty(key, value);
	}

	public void setAssetCQTags(String... tags) throws Exception {
		if (tags != null && tags.length > 0) {
			this._metaNode.setProperty("cq:tags", tags);
		}
	}

	/**
	 * Raw Binary Content would go here
	 * 
	 * @param contentValue
	 * @throws Exception
	 */
	public void setBinaryValue(Binary contentValue, String mimeType)
			throws Exception {
		// Create renditions folder node
		Node renditionsNode = this._contentNode.addNode("renditions",
				"nt:folder");
		// Add original image content node
		Node originalNode = renditionsNode.addNode("original", "nt:file");
		// Add content node for original
		Node originalNodeContent = originalNode.addNode("jcr:content",
				"nt:resource");
		originalNodeContent.setProperty("jcr:mimeType", mimeType);
		originalNodeContent.setProperty("jcr:data", contentValue);
		this._contentNode.setProperty("jcr:lastModified",
				Calendar.getInstance());
	}

	private void checkAddStringProperty(String propertyName, String value)
			throws Exception {
		if (!StringUtils.isEmpty(value)) {
			try {
				this._metaNode.setProperty(propertyName, value);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
