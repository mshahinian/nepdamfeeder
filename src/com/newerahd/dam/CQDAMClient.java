package com.newerahd.dam;

import java.io.File;
import java.io.FileInputStream;
import java.util.Calendar;

import javax.jcr.AccessDeniedException;
import javax.jcr.Binary;
import javax.jcr.InvalidItemStateException;
import javax.jcr.ItemExistsException;
import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.ReferentialIntegrityException;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import javax.jcr.ValueFactory;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.nodetype.NoSuchNodeTypeException;
import javax.jcr.nodetype.NodeType;
import javax.jcr.version.VersionException;

import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.log4j.Logger;

import com.newerahd.dam.model.CQDAMDocument;

public class CQDAMClient {
	Session cqSession = null;
	String userName = null;

	private static Logger logger = Logger.getLogger(CQDAMClient.class);

	/**
	 * Constructor
	 * 
	 * @param hostname
	 * @param port
	 * @param user
	 * @param password
	 * @param workspace
	 * @throws RepositoryException
	 */
	public CQDAMClient(String hostname, Integer port, String user,
			String password, String workspace) throws RepositoryException {
		new CQDAMClient("http://" + hostname + ":" + port, user, password,
				workspace);
	}

	/**
	 * 
	 * @param hostname
	 *            -- Example: http://cq.newerahd.com
	 * @param user
	 * @param password
	 * @param workspace
	 * @throws RepositoryException
	 */
	public CQDAMClient(String hostname, String user, String password,
			String workspace) throws RepositoryException {
		try {

			// RMI Connection
			Repository repository = JcrUtils.getRepository(hostname
					+ "/crx/server");

			// Workspace login
			SimpleCredentials creds = new SimpleCredentials(user,
					password.toCharArray());
			try {
				cqSession = repository.login(creds, workspace);
				this.userName = user;
				logger.info("Connected To Workspace: "
						+ cqSession.getWorkspace().getName());

			} catch (LoginException ex) {
				logger.error("Failed logging in to CRX repository at: "
						+ hostname, ex);
				ex.printStackTrace();
				throw ex;
			}
		} catch (RepositoryException re) {
			logger.error("Failed connecting to CRX repository at: " + hostname,
					re);
			re.printStackTrace();
			throw re;
		}

	}

	/**
	 * return Node from a path
	 * 
	 * @param path
	 *            the parent path in CRX that already exists
	 * @return Node object
	 * @throws PathNotFoundException
	 * @throws RepositoryException
	 */
	public Node getNodefromPath(String path) throws PathNotFoundException,
			RepositoryException {
		return cqSession.getNode(path);
	}

	/**
	 * Create a folder in the repository
	 * 
	 * @param parentNode
	 *            the parent path in CRX that already exists
	 * @param node
	 *            the name of the new folder to create
	 * 
	 * @throws Exception
	 *             if an error occurs while creating the node
	 */
	public Node createFolder(String parentNode, String childNode) throws Exception {
		Node parent = cqSession.getNode(parentNode);
		return this.createFolder(parent, childNode);
	}

	/**
	 * Create a folder in the repository
	 * 
	 * @param parentNode
	 * @param childNode
	 * @return
	 * @throws Exception
	 */
	public Node createFolder(Node parentNode, String childNode)	throws Exception {
		if (parentNode != null) {
			Node exists = null;
			try {
				exists = cqSession.getNode(parentNode.getPath() + "/" + childNode);
			} catch (PathNotFoundException pe) {
				exists = null;
				//logger.warn(String.format("Folder [%s] does not exist", pe.getMessage()));
			} 
			
			try {
				if (exists == null) {
					Node folder = parentNode.addNode(childNode, NodeType.NT_FOLDER);
					cqSession.save();
					logger.info("Created Folder: /" + parentNode.getName() + "/" + childNode);
					return folder;
				} else {
					return exists;
				}
			} catch (Exception e) {
				logger.error(
						"Failed creating CRX folder: /" + parentNode.getName() + "/" + childNode, e);
			}
		} else {
		   logger.error(String.format("Unable to Create parent folder [%s] -> [%s]" , parentNode, childNode));
		}
		return null;
	}

	public boolean folderExists(Node parentNode, String folder)
			throws Exception {

		if (parentNode.getNode(folder) != null) {
			return true;
		}
		return false;

	}

	/**
	 * Add an asset to the repository
	 * 
	 * @param parentNode
	 *            the CRX path to the parent node like:
	 *            /content/dam/new-era/conde-nast/foldername
	 * @param fileName
	 *            the name to use for the asset node that is being added.
	 *            Usually, this is a value such as: image1.png
	 * @param filePath
	 *            the path to the file on the local file system. This would be a
	 *            path such as: c:/temp/image1.png
	 * @param mimeType
	 *            the mime type of the asset being added. An example would be
	 *            image/png for a png file
	 * 
	 * @return the path of the asset in the repository after being added
	 */
	public String uploadAsset(String parentNode, String fileName,
			String filePath, String mimeType) {

		try {

			// Get InputStream to file
			File file = new File(filePath);
			FileInputStream is = new FileInputStream(file);

			// Set file type
			String fileType = "";
			if (mimeType.toLowerCase().endsWith("png")) {
				fileType = "PNG";
			} else if (mimeType.toLowerCase().endsWith("jpg")) {
				fileType = "JPG";
			}

			// Get handle to parent node
			Node node = cqSession.getNode(parentNode);
			ValueFactory valueFactory = cqSession.getValueFactory();
			Binary contentValue = valueFactory.createBinary(is);

			// Create asset node with name of file
			Node fileNode = node.addNode(fileName, "dam:Asset");
			// fileNode.addMixin("mix:referenceable");

			// Create content node under the asset
			Node contentNode = fileNode.addNode("jcr:content",
					"dam:AssetContent");
			contentNode.setProperty("jcr:lastModified", Calendar.getInstance());

			// Create metadata node for the asset content
			Node metaNode = contentNode.addNode("metadata", "nt:unstructured");
			metaNode.setProperty("dam:Bitsperpixel", 24);
			metaNode.setProperty("dam:Fileformat", fileType);
			metaNode.setProperty("dam:MIMEtype", mimeType);
			metaNode.setProperty("dam:Numberofimages", 1);
			metaNode.setProperty("dam:Numberoftextualcomments", 0);
			metaNode.setProperty("dam:Physicalheightindpi", -1);
			metaNode.setProperty("dam:Physicalheightininches", -1);
			metaNode.setProperty("dam:Physicalwidthindpi", -1);
			metaNode.setProperty("dam:Physicalwidthininches", -1);
			metaNode.setProperty("dam:Progressive", "no");
			metaNode.setProperty("dam:extracted", Calendar.getInstance());
			// metaNode.setProperty("dam:sha1",
			// "d5be05c4d42d934715ccb5cefadb0c513ec7a947"); // @ need this?
			metaNode.setProperty("dc:format", mimeType);
			metaNode.setProperty("dc:modified", Calendar.getInstance());
			metaNode.setProperty("jcr:lastModified", Calendar.getInstance());
			metaNode.setProperty("jcr:lastModifiedBy", this.userName);
			metaNode.addMixin("cq:Taggable");
			metaNode.setProperty("tiff:ImageLength", -1);
			metaNode.setProperty("tiff:ImageWidth", -1);

			// Create renditions folder node
			Node renditionsNode = contentNode
					.addNode("renditions", "nt:folder");

			// Add original image content node
			Node originalNode = renditionsNode.addNode("original", "nt:file");

			// Add content node for original
			Node originalNodeContent = originalNode.addNode("jcr:content",
					"nt:resource");
			originalNodeContent.setProperty("jcr:mimeType", mimeType);
			originalNodeContent.setProperty("jcr:data", contentValue);
			contentNode.setProperty("jcr:lastModified", Calendar.getInstance());

			cqSession.save();

			// Return the path to the document that was stored in CRX.
			return fileNode.getPath();
		} catch (Exception e) {
			logger.error("Failed creating CRX file: /" + parentNode + "/", e);
			e.printStackTrace();
		}

		return "";
	}
	
    public void feedCQDoc(CQDAMDocument cqdoc) throws AccessDeniedException, ItemExistsException, ReferentialIntegrityException, ConstraintViolationException, InvalidItemStateException, VersionException, LockException, NoSuchNodeTypeException, RepositoryException {
    	cqSession.save();   	
    }
    
    public void close() {
    	if (this.cqSession != null) {
    		cqSession.logout();
    	}
    }

	public Session getCqSession() {
		return cqSession;
	}

	/*
	public void feedAsset(Node parentNode) throws FileNotFoundException,
			RepositoryException {
		File file = new File("c:\\temp\\bwood.jpg");
		FileInputStream is = new FileInputStream(file);

		// Get handle to parent node
		// Node node = cqSession.getNode(parentNode);
		ValueFactory valueFactory = cqSession.getValueFactory();
		Binary contentValue = valueFactory.createBinary(is);

		// Create asset node with name of file
		Node fileNode = parentNode.addNode("Title Bwood", "dam:Asset");
		// fileNode.addMixin("mix:referenceable");

		// Create content node under the asset
		Node contentNode = fileNode.addNode("jcr:content", "dam:AssetContent");
		contentNode.setProperty("jcr:lastModified", Calendar.getInstance());

		// Create metadata node for the asset content
		Node metaNode = contentNode.addNode("metadata", "nt:unstructured");
		metaNode.setProperty("dam:Bitsperpixel", 24);
		metaNode.setProperty("dam:Fileformat", "JPG");
		metaNode.setProperty("dam:MIMEtype", "image/jpg");
		metaNode.setProperty("dam:Numberofimages", 1);
		metaNode.setProperty("dam:Numberoftextualcomments", 0);
		metaNode.setProperty("dam:Physicalheightindpi", -1);
		metaNode.setProperty("dam:Physicalheightininches", -1);
		metaNode.setProperty("dam:Physicalwidthindpi", -1);
		metaNode.setProperty("dam:Physicalwidthininches", -1);
		metaNode.setProperty("dam:Progressive", "no");

		metaNode.setProperty("dam:extracted", Calendar.getInstance());
		// metaNode.setProperty("dam:sha1",
		// "d5be05c4d42d934715ccb5cefadb0c513ec7a947"); // @ need this?
		metaNode.setProperty("dc:format", "image/jpg");
		metaNode.setProperty("dc:modified", Calendar.getInstance());
		metaNode.setProperty("jcr:lastModified", Calendar.getInstance());
		metaNode.setProperty("jcr:lastModifiedBy", this.userName);
		metaNode.addMixin("cq:Taggable");
		metaNode.setProperty("dc:title", "Supah Title");
		metaNode.setProperty("dc:rights", "windows xp");
		metaNode.setProperty("dc:description", "Spuah Dupah Description");
		metaNode.setProperty("cq:tags", new String[] { "style/color",
				"properties:style/color" });
		metaNode.setProperty("tiff:ImageLength", -1);
		metaNode.setProperty("tiff:ImageWidth", -1);

		// Create renditions folder node
		Node renditionsNode = contentNode.addNode("renditions", "nt:folder");

		// Add original image content node
		Node originalNode = renditionsNode.addNode("original", "nt:file");

		// Add content node for original
		Node originalNodeContent = originalNode.addNode("jcr:content",
				"nt:resource");
		originalNodeContent.setProperty("jcr:mimeType", "image/jpg");
		originalNodeContent.setProperty("jcr:data", contentValue);
		contentNode.setProperty("jcr:lastModified", Calendar.getInstance());

		cqSession.save();
		

	}*/

}
