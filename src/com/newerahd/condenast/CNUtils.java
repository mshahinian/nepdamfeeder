package com.newerahd.condenast;

import java.io.File;
import java.text.DateFormatSymbols;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

public class CNUtils {
	private static final Pattern NONLATIN = Pattern.compile("[^\\w-]");
	private static final Pattern WHITESPACE = Pattern.compile("[\\s]");

	public static String getMonth(int month) {
		return new DateFormatSymbols().getMonths()[month - 1];
	}

	public static void IncrementErrorCount(Map<String, Integer> errors, String errorLabel) {
		Integer cnt = errors.get(errorLabel);
		if (cnt == null) {
			errors.put(errorLabel, 1);
		} else {
			errors.put(errorLabel, cnt + 1);
		}
	}

	/**
	 * Convert Conde Nast ID to NEP ID
	 * 
	 * @param cnID
	 *            - conde nast ID
	 * @param cnPublication
	 *            - conde nast publication
	 * @param maxLen
	 *            - max length of the id to generate
	 * @return - new era id as string
	 * @throws Exception
	 */
	public static String generateNEPID(String cnID, String cnPublication, int maxLen) throws Exception {
		String originalID = cnID;
		cnID = cnID.toUpperCase();
		String[] prefixes = { "CN", "FP" };
		if (StringUtils.isEmpty(cnID)) {
			throw new Exception("The doc ID can't be empty or null");
		}
		if (StringUtils.isEmpty(cnPublication)) {
			throw new Exception("The cnPublication can't be empty or null");
		}

		/* Truncate if it already starts with CN prefix */
		for (String prefix : prefixes) {
			if (cnID.indexOf(prefix) == 0) {
				cnID = cnID.substring(2);
				break;
			}
		}

		/* Truncate leading zeros */
		cnID = cnID.replaceFirst("^0+(?!$)", "");

		/* Figure out 2 letter acronym */
		String acro = cnPubToAcro(cnPublication);
		if (StringUtils.isEmpty(acro)) {
			throw new Exception("Unrecognized Publication for " + originalID + " => " + cnPublication);
		}

		int cnIDLen = cnID.length();
		int estimatedLength = 2 + cnIDLen + acro.length();

		StringBuilder sb = new StringBuilder();
		sb.append("CN");
		sb.append(cnID);

		if (estimatedLength < maxLen) { // pad with Ws
			for (int i = 0; i < (maxLen - estimatedLength); i++) {
				sb.append("W");
			}
		} /*
		 * else if (estimatedLength < maxLen) { sb.append(cnID.substring(0,
		 * cnID.length() - acro.length())); }
		 */
		sb.append(acro);
		return sb.toString();
	}

	public static String cnPubToAcro(String cnPublication) {
		switch (cnPublication.toLowerCase()) {
		case "the new yorker covers":
			return "NC";
		case "the new yorker cartoons":
			return "CB";
		case "vogue":
			return "VG";
		case "vanity fair":
			return "VF";
		case "house & garden":
			return "HG";
		case "gourmet":
			return "GM";
		case "gq":
			return "GQ";
		case "mademoiselle":
			return "MM";
		case "glamour":
			return "GL";
		case "charm":
			return "CH";
		case "brides":
			return "BD";
		case "conde nast":
			return "CN";
		case "wwd":
			return "WD";
		default:
			return null;
		}
	}

	public static String toSlug(String input) {
		String nowhitespace = WHITESPACE.matcher(input).replaceAll("-");
		String normalized = Normalizer.normalize(nowhitespace, Form.NFD);
		String slug = NONLATIN.matcher(normalized).replaceAll("");
		return slug.toLowerCase(Locale.ENGLISH);
	}

	public static String getFileExtension(String fileName) {
		int p = fileName.lastIndexOf('.');
		return (p > 0) ? fileName.substring(p + 1) : "";
	}

	/**
	 * Locate actual binary image file. Given wizards at conde nast we can get
	 * an image for a cnID named as cnID.jpg, cnID.tiff, TCB-cnID.jpg,
	 * TCB-cnID.tiff so we have to check for all the combinations.
	 * 
	 * @param imageRootPath
	 *            - root image path
	 * @param cnID
	 *            - Conde Nast ID
	 * @param nepID
	 * @return File handle
	 */
	public static File locateNEPFile(String imageRootPath, String nepID) {
		String fullFilePath = genFilePath(imageRootPath, nepID, "", ".tif");
		File assetFile = new File(fullFilePath);
		return assetFile;
	}

	public static File locateCNFile(String imageRootPath, String cnID) {
		String fullFilePath = genFilePath(imageRootPath, cnID, "", ".tif");
		System.out.println("Trying :" + fullFilePath);
		File assetFile = new File(fullFilePath);
		if (!assetFile.getAbsoluteFile().exists()) {
			fullFilePath = genFilePath(imageRootPath, cnID, "TCB-", ".tif");
			assetFile = new File(fullFilePath);
			if (!assetFile.getAbsoluteFile().exists()) {
				fullFilePath = genFilePath(imageRootPath, cnID, "", ".jpg");
				assetFile = new File(fullFilePath);
				if (!assetFile.getAbsoluteFile().exists()) {
					fullFilePath = genFilePath(imageRootPath, cnID, "TCB-", ".jpg");
					assetFile = new File(fullFilePath);
				}
			}
		}
		System.out.println(assetFile.getAbsolutePath());
		return assetFile;
	}

	public static String genFilePath(String imageRootPath, String cnID, String prefix, String suffix) {
		return imageRootPath + File.separator + prefix + cnID + suffix;
	}

	public static String genCNTitle(String pubName, String pubDate) {
		return pubName + " - " + pubDate;
	}

	public static String swapFirstLastName(String input, char separator) {
		if (!StringUtils.isEmpty(input)) {
			int ls = input.lastIndexOf(' ');
			if (ls > -1) {
				return String.format("%s%s %s", input.substring(ls + 1, input.length()), separator, input.subSequence(0, ls));
			}
		}
		return input;
	}

	public static String cnIDFromFileName(String fileName) {
		String fn = fileName.substring(0, fileName.lastIndexOf('.'));
		int ui = fn.indexOf('_');
		return (ui > 0) ? fn.substring(0, ui) : fn;
	}

	/**
	 * Generate a full slug based on author, title and id. Since there are some
	 * duplicate titles we need to mix in ID to avoid duplicate slugs
	 * 
	 * @param author
	 * @param title
	 * @param id
	 * @return slug
	 */
	public static String generateFullSlug(String author, String title, String id) {
		String authSlug = CNUtils.toSlug(author);
		title = title.trim().replace(" - ", " ");
		title = title + " " + id.replaceAll("\\D+", "");
		title = CNUtils.toSlug(title);
		return String.format("%s/%s", authSlug, title);
	}

	/**
	 * If a string contains multiple Authors delimited return the first one
	 * 
	 * @param authorString
	 * @return String with author Name
	 */
	public static String extractFirstAuthorName(String authorString, String delimiter) {
		String[] a = authorString.split(delimiter);
		return (a.length > 0) ? a[0] : authorString;
	}

	public static String removeLineBreaks(String input) {
		return input.replace("\n", "").replace("\r", "").trim();
	}

	public static Calendar dateToCalendar(Date dobj) {
		Calendar pubDate = Calendar.getInstance();
		if (dobj != null) {
			pubDate.setTime(dobj);
		} else {
			pubDate.set(Calendar.YEAR, 1800);
			pubDate.set(Calendar.MONTH, 1);
			pubDate.set(Calendar.DAY_OF_MONTH, 1);
			pubDate.set(Calendar.HOUR, 6);
			pubDate.set(Calendar.MINUTE, 0);
			pubDate.setTimeZone(TimeZone.getTimeZone("CST"));
		}

		return pubDate;
	}
}
