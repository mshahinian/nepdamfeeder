package com.newerahd.condenast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

public class CSVUtils {

	@SuppressWarnings("resource")
	public static CSVPrinter initCSVPrinter(String csvOutputPath)
			throws IOException {

		FileOutputStream fos = new FileOutputStream(csvOutputPath);
		OutputStreamWriter csvWriter = new OutputStreamWriter(fos, "UTF-8");
		CSVPrinter csvPrinter = new CSVPrinter(csvWriter, CSVFormat.EXCEL);
		csvPrinter.print("products_imagecode");
		csvPrinter.print("full_slug");
		csvPrinter.print("artist_slug");
		csvPrinter.print("keyword");
		csvPrinter.print("subjects_name");
		csvPrinter.print("description");
		csvPrinter.print("artist");
		csvPrinter.print("title");
		csvPrinter.print("orientation");
		csvPrinter.print("color_primary");
		csvPrinter.print("color_secondary");
		csvPrinter.print("color_family");
		csvPrinter.print("vendor_sku");
		csvPrinter.print("products_artist_release_date");
		csvPrinter.print("collection");
		csvPrinter.print("partner_id");
		csvPrinter.println();
		return csvPrinter;
	}

	public static void closeCSVPrinter(CSVPrinter csvPrinter, OutputStreamWriter csvWriter, FileOutputStream fos) {
		try {
			csvPrinter.flush();
			csvWriter.flush();
			fos.flush();
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
