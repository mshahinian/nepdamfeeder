package com.newerahd.condenast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFactory;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.newerahd.colordetect.ColorDetector;
import com.newerahd.config.FeederConfig;
import com.newerahd.dam.CQDAMClient;
import com.newerahd.dam.model.CQDAMDocument;

public class NewYorkerConverter {
	public static Logger logger = Logger.getLogger(NewYorkerConverter.class);
	public static String publicationPrefix = "The New Yorker";
	public static Pattern nyDescPattern = Pattern.compile("\\((.*?)\\)");

	// formatter for mysql
	SimpleDateFormat mysqldf = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * Process New Yorker Data
	 * 
	 * @param fileName
	 *            - path to the csv file
	 * @param publicationSuffix
	 *            - publication name
	 * @param imageRootPath
	 *            - path to the images
	 * @return
	 * @throws Exception
	 */
	public static Map<String, Integer> handleNewYorker(String fileName, String publicationSuffix, String imageRootPath)
			throws Exception {
		CSVParser parser;
		int counter = 0;

		Map<String, Integer> errors = new HashMap<String, Integer>();

		parser = new CSVParser(new FileReader(fileName), CSVFormat.EXCEL);
		CQDAMClient cqClient = new CQDAMClient(FeederConfig.cqServer, FeederConfig.cqUser, FeederConfig.cqPass,
				FeederConfig.cqDefaultWorkspace);
		Node source = cqClient.createFolder(FeederConfig.cqRootFolder, publicationPrefix);
		Node rootFolder = cqClient.createFolder(source, publicationSuffix);

		for (CSVRecord record : parser) {
			try {
				CNUtils.IncrementErrorCount(errors, "TotalCount");

				Calendar pubDate = handlePublicationDate(record.get(2));

				if (pubDate != null) {
					String year = Integer.toString(pubDate.get(Calendar.YEAR));
					Integer month = pubDate.get(Calendar.MONTH);
					String fullPublicationName = publicationPrefix + " " + publicationSuffix;
					String cnID = StringUtils.trim(record.get(8));
					String nepID = CNUtils.generateNEPID(cnID, fullPublicationName, 10);
					String title = generateNewYorkerTitle(record.get(3), month + 1, year);
					String description = CNUtils.removeLineBreaks(record.get(4));
					String keywords = CNUtils.removeLineBreaks(record.get(5).replace(cnID, "").replace("artkey", "")
							.replace(";", ","));
					String creator = CNUtils.extractFirstAuthorName(record.get(6).trim(), ";");
					// String fullSlug = CNUtils.generateFullSlug(creator,
					// title, cnID);

					String colorFamily = "";
					String primaryColorHex = "";
					String secondaryColorHex = "";
					String orientation = "";

					/*
					 * Conde Nast concatenated descriptions and keywords in a
					 * single field - so we need to separate them and stick them
					 * in appropriate fields
					 */
					Matcher m = nyDescPattern.matcher(keywords);
					while (m.find()) {
						String match = m.group();
						description += " " + match.replace("(", "").replace(")", "");
						keywords = StringUtils.remove(keywords, match);
					}

					/*
					 * File assetFile = CNUtils.locateCNFile(imageRootPath,
					 * nepID); if (assetFile.getAbsoluteFile().exists()) {
					 * 
					 * ColorDetector nepCD = new ColorDetector(assetFile);
					 * orientation = nepCD.getImageOrientation(); List<String>
					 * allPrimaryHexColors = new
					 * ArrayList<String>(nepCD.getPrimaryHexColors());
					 * allPrimaryHexColors
					 * .addAll(nepCD.getColorFamilyHexColors());
					 * 
					 * primaryColorHex =
					 * StringUtils.join(allPrimaryHexColors,',').toUpperCase();
					 * secondaryColorHex =
					 * StringUtils.join(nepCD.getSecondaryHexColors
					 * (),',').toUpperCase(); colorFamily =
					 * StringUtils.join(nepCD.getColorFamilyNames(),',');
					 * logger.info(String.format("%s %s",
					 * assetFile.getAbsolutePath(), colorFamily )); }
					 */

					Node yearNode = cqClient.createFolder(rootFolder, year);
					Node moNode = cqClient.createFolder(yearNode, CNUtils.getMonth(month + 1));

					CQDAMDocument cqDoc = new CQDAMDocument(moNode, nepID);
					cqDoc.setAssetTitle(title);

					cqDoc.setAssetDescription(description);
					cqDoc.setLastModifiedBy(FeederConfig.cqUser);
					cqDoc.setAssetAuthor(creator);
					cqDoc.addMetadata("dc:date", pubDate);
					cqDoc.addMetadata("dc:publisher", publicationPrefix + " " + publicationSuffix);
					cqDoc.addMetadata("dc:subject", keywords);
					cqDoc.addMetadata("dam:externalid", cnID);
					cqDoc.addMetadata("dc:identifier", nepID);

					/** Handle the binary file **/

					File assetFile = CNUtils.locateCNFile(imageRootPath, nepID);

					if (assetFile.exists()) {
						FileInputStream is = new FileInputStream(assetFile);
						ValueFactory valueFactory = cqClient.getCqSession().getValueFactory();
						Binary contentValue = valueFactory.createBinary(is);
						cqDoc.setBinaryValue(contentValue, "image/jpg");
					}

					cqClient.feedCQDoc(cqDoc);
					++counter;

					if (counter % 200 == 0) {
						logger.info(String.format("Processed %s images", counter));
					}
				}

			} catch (Exception e) {
				logger.error("Problem occured " + e.getMessage());
				e.printStackTrace();
			}

		}
		cqClient.close();
		return errors;
	}

	/**
	 * Convert from CSV supplied by conde Nast to NEP format so it can be
	 * imported into mysql
	 * 
	 * @param record
	 *            - CSV record
	 * @param publicationSuffix
	 *            - "Covers or Cartoons"
	 * @return - CSV record
	 * @throws Exception
	 */
	public String[] cnCSVTOnepCSV(CSVRecord record, String publicationSuffix, String imageRootPath) throws Exception {

		Calendar pubDate = handlePublicationDate(record.get(2));

		if (pubDate != null) {
			String year = Integer.toString(pubDate.get(Calendar.YEAR));
			String fullPublicationName = publicationPrefix + " " + publicationSuffix;
			String cnID = StringUtils.trim(record.get(8));
			String nepID = CNUtils.generateNEPID(cnID, fullPublicationName, 10);
			String title = generateNewYorkerTitle(record.get(3), pubDate.get(Calendar.MONTH) + 1, year);
			String description = CNUtils.removeLineBreaks(record.get(4));
			String keywords = CNUtils.removeLineBreaks(record.get(5).replace(cnID, "").replace("artkey", "").replace(";", ","));
			String creator = CNUtils.extractFirstAuthorName(record.get(6).trim(), ";");
			String fullSlug = CNUtils.generateFullSlug(creator, title, cnID);

			String colorFamily = "";
			String primaryColorHex = "";
			String secondaryColorHex = "";
			String orientation = "";

			/*
			 * Conde Nast concatenated descriptions and keywords in a single
			 * field - so we need to separate them and stick them in appropriate
			 * fields
			 */
			Matcher m = nyDescPattern.matcher(keywords);
			while (m.find()) {
				String match = m.group();
				description += " " + match.replace("(", "").replace(")", "");
				keywords = StringUtils.remove(keywords, match);
			}

			File assetFile = CNUtils.locateCNFile(imageRootPath, nepID);
			if (assetFile.getAbsoluteFile().exists()) {

				ColorDetector nepCD = new ColorDetector(assetFile);
				orientation = nepCD.getImageOrientation();
				/* Merge detected primary colors with spectrum primary colors */
				List<String> allPrimaryHexColors = new ArrayList<String>(nepCD.getPrimaryHexColors());
				allPrimaryHexColors.addAll(nepCD.getColorFamilyHexColors());

				primaryColorHex = StringUtils.join(allPrimaryHexColors, ',').toUpperCase();
				secondaryColorHex = StringUtils.join(nepCD.getSecondaryHexColors(), ',').toUpperCase();
				colorFamily = StringUtils.join(nepCD.getColorFamilyNames(), ',');
				logger.info(String.format("%s %s", assetFile.getAbsolutePath(), colorFamily));
			}

			List<String> values = new ArrayList<String>();
			values.add(nepID);
			values.add(fullSlug);
			values.add(CNUtils.toSlug(creator));
			values.add(keywords);
			values.add(fullPublicationName);
			values.add(description);
			values.add(CNUtils.swapFirstLastName(creator, ','));
			values.add(title);
			values.add(orientation);
			values.add(primaryColorHex);
			values.add(secondaryColorHex);
			values.add(colorFamily);
			values.add(cnID);
			values.add(mysqldf.format(pubDate.getTime()));
			values.add("");
			values.add("162");
			return values.toArray(new String[values.size()]);
		}

		return null;
	}

	public void renameImageFile(CSVRecord record, String baseImagePath, String publicationName) throws Exception {

		String fullPublicationName = publicationPrefix + " " + publicationName;
		String cnID = StringUtils.trim(record.get(8));
		String nepID = CNUtils.generateNEPID(cnID, fullPublicationName, 10);

		File assetFile = CNUtils.locateCNFile(baseImagePath, cnID);
		if (assetFile.getAbsoluteFile().exists()) {
			// logger.info(String.format("Original => %s => %s",
			// assetFile.getName(), assetFile.getParent()));

			String newFile = assetFile.getParent() + File.separator + nepID + "." + CNUtils.getFileExtension(assetFile.getName());
			boolean renamed = false;
			try {
				renamed = assetFile.renameTo(new File(newFile));
				System.out.println("Renaming to: " + newFile);
			} catch (SecurityException ex) {
				logger.error(String.format("Unable to rename %s ", newFile, ex.getMessage()));
			}
			if (renamed) {
				logger.info(String.format("Renamed %s => %s", assetFile.getName(), newFile));
			}
		} else {
			logger.error("Unable to locate File for cnID " + cnID);
		}
	}

	/**
	 * Convert CN date String into Calendar
	 * 
	 * @param dateString
	 *            - input string from CSV
	 * @return Calendar
	 */
	public static Calendar handlePublicationDate(String dateString) {
		String[] pubinfo = (StringUtils.isEmpty(dateString)) ? null : StringUtils.split(dateString, ',');

		if (pubinfo != null) {
			Integer iYear = 1800;
			Integer iDay = 1;
			Integer iMonth = 1;
			if (pubinfo.length == 3) {
				iYear = Integer.parseInt(pubinfo[2]);
				iDay = Integer.parseInt(pubinfo[1]);
				iMonth = Integer.parseInt(pubinfo[0]);
			}

			Calendar pubDate = Calendar.getInstance();
			pubDate.set(Calendar.YEAR, iYear);
			pubDate.set(Calendar.MONTH, iMonth - 1);
			pubDate.set(Calendar.DAY_OF_MONTH, iDay);
			pubDate.set(Calendar.HOUR, 6);
			pubDate.set(Calendar.MINUTE, 0);
			pubDate.setTimeZone(TimeZone.getTimeZone("CST"));
			return pubDate;
		}

		return null;
	}

	/**
	 * Generate a title for new yorker publication
	 * 
	 * @param currentTitle
	 *            - current Title assigned by conde nast
	 * @param month
	 * @param year
	 * @return formatted title
	 */
	public static String generateNewYorkerTitle(String currentTitle, int month, String year) {
		currentTitle = currentTitle.trim();
		if (StringUtils.isEmpty(currentTitle) || currentTitle.toLowerCase().trim().equals("no data")) {
			return publicationPrefix + " - " + CNUtils.getMonth(month) + ", " + year;
		}
		return currentTitle;
	}

	public static void NYCoverCSVRecordToCQDAM(CSVRecord coverRecord) {

	}

}
