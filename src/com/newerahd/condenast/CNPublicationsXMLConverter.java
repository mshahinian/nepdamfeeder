package com.newerahd.condenast;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.ValueFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.newerahd.colordetect.ColorDetector;
import com.newerahd.config.FeederConfig;
import com.newerahd.dam.CQDAMClient;
import com.newerahd.dam.model.CQDAMDocument;

public class CNPublicationsXMLConverter {
	public static Logger logger = Logger.getLogger(CNPublicationsXMLConverter.class);
	DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder builder;
	XPath xpath;
	XPathExpression id;
	XPathExpression cnid;
	XPathExpression create_date;
	XPathExpression small_thumb;
	XPathExpression medium_thumb;
	XPathExpression large_thumb;
	XPathExpression cnissue_date;
	XPathExpression cncover_date;
	XPathExpression cnpublication;
	XPathExpression cncreator;
	XPathExpression cncolor;
	XPathExpression cnpictured;
	XPathExpression keywords;
	XPathExpression description;
	XPathExpression cncategories;

	public CNPublicationsXMLConverter() throws ParserConfigurationException, XPathExpressionException {
		domFactory.setNamespaceAware(true);
		builder = domFactory.newDocumentBuilder();
		xpath = XPathFactory.newInstance().newXPath();
		id = xpath.compile("//Id/text()");
		cnid = xpath.compile("//Content/Metadata/ItemNumber/text()");
		create_date = xpath.compile("//CreatedAt/text()");
		small_thumb = xpath.compile("//Content/Metadata/SmallThumbnail/text()");
		medium_thumb = xpath.compile("//Content/Metadata/MediumThumbnail/text()");
		large_thumb = xpath.compile("//Content/Metadata/WatermarkedPreview/text()");
		cnissue_date = xpath.compile("//Content/Metadata/IssueDate/text()");
		cncover_date = xpath.compile("//Content/Metadata/CoverDisplayDate/text()");
		cnpublication = xpath.compile("//Content/Metadata/Publication/text()");
		cncreator = xpath.compile("//Content/Metadata/Creator/text()");
		cnpictured = xpath.compile("//Content/Metadata/Pictured/text()");
		description = xpath.compile("//Content/Metadata/Description/text()");
		cncolor = xpath.compile("//Content/Metadata/Color/text()");
		keywords = xpath.compile("//Content/Metadata/KeywordsInternal/text()");
		cncategories = xpath.compile("//Content/Metadata/Categories/Category");
	}

	public String getCNDocID(String fileName) throws Exception {

		try {
			Document xmlDoc = builder.parse(fileName);
			return (String) cnid.evaluate(xmlDoc, XPathConstants.STRING);

		} catch (SAXException sax) {
			logger.error(String.format("[%s] => [%s]", fileName, sax.getMessage()));
		} catch (IOException e) {
			logger.error(String.format("[%s] => [%s]", fileName, e.getMessage()));
		} catch (XPathExpressionException e) {
			logger.error(String.format("[%s] => [%s]", fileName, e.getMessage()));
		}

		return null;
	}

	/**
	 * Renames Original CN File to a file name with NEP ID
	 * 
	 * @param fileName
	 *            - xml file name with Conde Metadata
	 * @param imageRootPath
	 *            - directory where the images are located
	 * @throws Exception
	 */
	public void renameCNFile(String fileName, String imageRootPath) throws Exception {
		Document xmlDoc = builder.parse(fileName);
		String cnID = (String) cnid.evaluate(xmlDoc, XPathConstants.STRING);
		String cnPublication = (String) cnpublication.evaluate(xmlDoc, XPathConstants.STRING);

		/* default to conde nast */
		if (StringUtils.isEmpty(cnPublication)) {
			cnPublication = "Conde Nast";
		}

		File assetFile = CNUtils.locateCNFile(imageRootPath, cnID);
		if (assetFile.getAbsoluteFile().exists()) {
			// logger.info(String.format("Original => %s => %s",
			// assetFile.getName(), assetFile.getParent()));
			String nepID = CNUtils.generateNEPID(cnID, cnPublication, 10);
			String newFile = assetFile.getParent() + File.separator + nepID + "." + CNUtils.getFileExtension(assetFile.getName());
			boolean renamed = false;
			try {
				renamed = assetFile.renameTo(new File(newFile));
			} catch (SecurityException ex) {
				logger.error(String.format("Unable to rename %s ", newFile, ex.getMessage()));
			}
			if (renamed) {
				logger.info(String.format("Renamed %s => %s", assetFile.getName(), newFile));
			}
		} else {
			logger.error("Unable to locate File for cnID " + cnID);
		}
	}

	public String[] cnXMLtoCSV(String fileName, String imageRootPath) throws Exception {
		Document xmlDoc = builder.parse(fileName);
		String cnID = (String) cnid.evaluate(xmlDoc, XPathConstants.STRING);
		String cnPublication = (String) cnpublication.evaluate(xmlDoc, XPathConstants.STRING);

		if (StringUtils.isEmpty(cnPublication)) {
			cnPublication = "conde nast";
		}
		String nepID = "";
		try {
			nepID = CNUtils.generateNEPID(cnID, cnPublication, 10);
		} catch (Exception ex) {
			System.out.println("NEP ID " + ex.getMessage());
		}
		String cnCoverDate = (String) cncover_date.evaluate(xmlDoc, XPathConstants.STRING);
		String title = CNUtils.genCNTitle(cnPublication, cnCoverDate);
		String creator = CNUtils.extractFirstAuthorName((String) cncreator.evaluate(xmlDoc, XPathConstants.STRING), ";");
		String description_s = CNUtils.removeLineBreaks(((String) description.evaluate(xmlDoc, XPathConstants.STRING)));
		String keywords_s =  CNUtils.removeLineBreaks(((String) keywords.evaluate(xmlDoc, XPathConstants.STRING)).replace(";", ","));
		String dateString = (String) cnissue_date.evaluate(xmlDoc, XPathConstants.STRING);
		String fullSlug = CNUtils.generateFullSlug(creator, title, cnID);

		String colorFamily = "";
		String primaryColorHex = "";
		String secondaryColorHex = "";
		String orientation = "";

		String[] cats = cnCategoriesToStringArr(xmlDoc);
		String categories = (cats.length > 0) ? StringUtils.join(cats, ',') : "";

		File assetFile = CNUtils.locateCNFile(imageRootPath, cnID);
		if (assetFile.getAbsoluteFile().exists()) {

			ColorDetector nepCD = new ColorDetector(assetFile);
			orientation = nepCD.getImageOrientation();
			/* Merge detected primary colors with spectrum primary colors */
			List<String> allPrimaryHexColors = new ArrayList<String>(nepCD.getPrimaryHexColors());
			allPrimaryHexColors.addAll(nepCD.getColorFamilyHexColors());

			primaryColorHex = StringUtils.join(allPrimaryHexColors, ',').toUpperCase();
			secondaryColorHex = StringUtils.join(nepCD.getSecondaryHexColors(), ',').toUpperCase();
			colorFamily = StringUtils.join(nepCD.getColorFamilyNames(), ',');
			logger.info(String.format("%s %s", assetFile.getAbsolutePath(), colorFamily));
		}

		List<String> values = new ArrayList<String>();
		values.add(nepID);
		values.add(fullSlug);
		values.add(CNUtils.toSlug(creator));
		values.add(keywords_s);
		values.add(cnPublication);
		values.add(description_s);
		values.add(CNUtils.swapFirstLastName(creator, ','));
		values.add(title);
		values.add(orientation);
		values.add(primaryColorHex);
		values.add(secondaryColorHex);
		values.add(colorFamily);
		values.add(cnID);
		values.add(dateString);
		values.add(categories);
		values.add("162");

		return values.toArray(new String[values.size()]);
	}

	public void feedCNPublication(String fileName, String imageRootPath, CQDAMClient cqClient) throws Exception {
		try {
			Document xmlDoc = builder.parse(fileName);
			String cnID = (String) cnid.evaluate(xmlDoc, XPathConstants.STRING);
			String cnPublication = (String) cnpublication.evaluate(xmlDoc, XPathConstants.STRING);

			if (StringUtils.isEmpty(cnPublication)) {
				cnPublication = "conde nast";
			}
			String nepID = "";
			try {
				nepID = CNUtils.generateNEPID(cnID, cnPublication, 10);
			} catch (Exception ex) {
				System.out.println("NEP ID " + ex.getMessage());
			}
			String cnCoverDate = (String) cncover_date.evaluate(xmlDoc, XPathConstants.STRING);
			String title = CNUtils.genCNTitle(cnPublication, cnCoverDate);
			String creator = CNUtils.extractFirstAuthorName((String) cncreator.evaluate(xmlDoc, XPathConstants.STRING), ";");
			String description_s = CNUtils.removeLineBreaks(((String) description.evaluate(xmlDoc, XPathConstants.STRING)));
			String keywords_s =  CNUtils.removeLineBreaks(((String) keywords.evaluate(xmlDoc, XPathConstants.STRING)).replace(";", ","));
			String dateString = (String) cnissue_date.evaluate(xmlDoc, XPathConstants.STRING);
			/*String fullSlug = CNUtils.generateFullSlug(creator, title, cnID);

			String colorFamily = "";
			String primaryColorHex = "";
			String secondaryColorHex = "";
			String orientation = "";
			*/

			String[] cats = cnCategoriesToStringArr(xmlDoc);
			String tags = (cats.length > 0) ? StringUtils.join(cats, ',') : "";

			/*File assetFile = CNUtils.locateCNFile(imageRootPath, cnID);
			if (assetFile.getAbsoluteFile().exists()) {

				ColorDetector nepCD = new ColorDetector(assetFile);
				orientation = nepCD.getImageOrientation();
				List<String> allPrimaryHexColors = new ArrayList<String>(nepCD.getPrimaryHexColors());
				allPrimaryHexColors.addAll(nepCD.getColorFamilyHexColors());

				primaryColorHex = StringUtils.join(allPrimaryHexColors, ',').toUpperCase();
				secondaryColorHex = StringUtils.join(nepCD.getSecondaryHexColors(), ',').toUpperCase();
				colorFamily = StringUtils.join(nepCD.getColorFamilyNames(), ',');
				logger.info(String.format("%s %s", assetFile.getAbsolutePath(), colorFamily));
			}*/
			

			Node pubMonthNode = null;
			Date cnPubDate = null;
			try {
				cnPubDate = DateUtils.parseDate(dateString, new String[] { "yyyy-mm-dd" });

				String pubYear = StringUtils.isEmpty(dateString) ? "1800" : dateString.substring(0, 4);
				String pubMonth = StringUtils.isEmpty(dateString) ? "January" : CNUtils.getMonth((Integer.parseInt(dateString
						.substring(5, 7))));

				Node pubNode = cqClient.createFolder(FeederConfig.cqRootFolder, cnPublication);
				Node pubYearNode = cqClient.createFolder(pubNode, pubYear);
				pubMonthNode = cqClient.createFolder(pubYearNode, pubMonth);

			} catch (ParseException e) {
				logger.error(String.format("Invalid Date [%s] - [%s]", dateString, cnID));
			}

		

			CQDAMDocument cqDoc = new CQDAMDocument(pubMonthNode, nepID);
			cqDoc.setAssetDescription(description_s);
			cqDoc.setAssetTitle(title);
			cqDoc.setAssetAuthor(creator);

			cqDoc.setLastModifiedBy(FeederConfig.cqUser);
			if (cnPubDate != null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(cnPubDate);
				cqDoc.addMetadata("dc:date", cal);
			}

			cqDoc.addMetadata("dc:publisher", cnPublication);
			cqDoc.addMetadata("dc:subject", keywords_s);
			cqDoc.addMetadata("dam:externalid", cnID);
			cqDoc.addMetadata("dc:identifier", nepID);

			File assetFile = CNUtils.locateCNFile(imageRootPath, nepID);

			if (assetFile.exists()) {
				FileInputStream is = new FileInputStream(assetFile);
				ValueFactory valueFactory = cqClient.getCqSession().getValueFactory();
				Binary contentValue = valueFactory.createBinary(is);
				cqDoc.setBinaryValue(contentValue, "image/tiff");
			}

		} catch (SAXException sax) {
			logger.error(String.format("[%s] => [%s]", fileName, sax.getMessage()));
		} catch (IOException e) {
			logger.error(String.format("[%s] => [%s]", fileName, e.getMessage()));
		} catch (XPathExpressionException e) {
			logger.error(String.format("[%s] => [%s]", fileName, e.getMessage()));
		}

	}

	private String[] cnCategoriesToStringArr(Document xmlDoc) throws XPathExpressionException {
		List<String> cl = new ArrayList<String>();
		NodeList nd = (NodeList) cncategories.evaluate(xmlDoc, XPathConstants.NODESET);
		for (int i = 0; i < nd.getLength(); i++) {
			org.w3c.dom.Node n = nd.item(i);
			cl.add(n.getTextContent().replace(":", ""));
		}
		return cl.toArray(new String[cl.size()]);
	}

}
