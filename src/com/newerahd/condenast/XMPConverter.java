package com.newerahd.condenast;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.ValueFactory;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.newerahd.config.FeederConfig;
import com.newerahd.dam.CQDAMClient;
import com.newerahd.dam.model.CQDAMDocument;

/**
 * This Class converts images with embedded xmp information
 * 
 * @author mshahinian
 * 
 */
public class XMPConverter {
	public static Logger logger = Logger.getLogger(XMPConverter.class);
	public int counter = 0;

	public String[] feedCNPublication(String fileName, String pubName, CQDAMClient cqClient) throws Exception {
		try {
			counter++;

			String keywords = "";
			String creator = "";
			String categories = "";
			String description = "";
			String city = "";
			String state = "";
			String loc = "";
			String subloc = "";
			String title = "";
			String source = "";
			Date createDate = null;
			File xmpFile = new File(fileName);

			Metadata metadata = ImageMetadataReader.readMetadata(xmpFile);
			Directory iptcDir = metadata.getDirectory(com.drew.metadata.iptc.IptcDirectory.class);
			Directory exif0 = metadata.getDirectory(com.drew.metadata.exif.ExifIFD0Directory.class);

			if (iptcDir != null) {
				keywords = iptcDir.getString(com.drew.metadata.iptc.IptcDirectory.TAG_KEYWORDS);
				creator = iptcDir.getString(com.drew.metadata.iptc.IptcDirectory.TAG_BY_LINE);
				categories = iptcDir.getString(com.drew.metadata.iptc.IptcDirectory.TAG_CATEGORY);
				description = iptcDir.getString(com.drew.metadata.iptc.IptcDirectory.TAG_CAPTION);
				createDate = iptcDir.getDate(com.drew.metadata.iptc.IptcDirectory.TAG_DATE_CREATED);
				city = iptcDir.getString(com.drew.metadata.iptc.IptcDirectory.TAG_CITY);
				state = iptcDir.getString(com.drew.metadata.iptc.IptcDirectory.TAG_PROVINCE_OR_STATE);
				loc = iptcDir.getString(com.drew.metadata.iptc.IptcDirectory.TAG_COUNTRY_OR_PRIMARY_LOCATION_NAME);
				subloc = iptcDir.getString(com.drew.metadata.iptc.IptcDirectory.TAG_SUB_LOCATION);
				title = iptcDir.getString(com.drew.metadata.iptc.IptcDirectory.TAG_OBJECT_NAME);
				source = iptcDir.getString(com.drew.metadata.iptc.IptcDirectory.TAG_SOURCE);
			} else if (exif0 != null) {

			}

			/*for (Directory directory : metadata.getDirectories()) {
			    for (com.drew.metadata.Tag tag : directory.getTags()) {
			        System.out.println(tag);
			    }
			}*/

			pubName = (StringUtils.isEmpty(source)) ? pubName : source;
			String cnID = CNUtils.cnIDFromFileName(xmpFile.getName());
			String nepID = CNUtils.generateNEPID(cnID, pubName, 10);

			title = (StringUtils.isEmpty(title)) ? nepID : title;
			String fullSlug = CNUtils.generateFullSlug(creator, title, cnID);

			logger.info(String.format("FN: %s, CN: %s NEP %s", xmpFile.getName(), cnID, nepID));
			System.out.println(cnID + "," + nepID);

			StringBuilder sb = new StringBuilder();
			if (!StringUtils.isEmpty(city)) {
				sb.append(city).append(',');
			}

			if (!StringUtils.isEmpty(state)) {
				sb.append(state).append(',');
			}

			if (!StringUtils.isEmpty(loc)) {
				sb.append(loc).append(',');
			}

			if (!StringUtils.isEmpty(subloc)) {
				sb.append(subloc).append(',');
			}

			String location = sb.toString();

			System.out.println(pubName);

			Calendar pubDate = CNUtils.dateToCalendar(createDate);

			/*
			 * System.out.println("keywords: " + keywords);
			 * System.out.println("artist: " + creator);
			 * System.out.println("description: " + description);
			 * System.out.println("title: " + title);
			 * System.out.println("createDate: " + createDate);
			 * System.out.println("cat: " + categories + " city: " + city +
			 * " state: " + state + " loc: " + loc + " subloc: " + subloc +
			 * " source: " + source);
			 */

			
			Node sNode = cqClient.createFolder(FeederConfig.cqRootFolder, pubName);

			Node yNode = cqClient.createFolder(sNode, Integer.toString(pubDate.get(Calendar.YEAR)));
			Node moNode = cqClient.createFolder(yNode, CNUtils.getMonth(pubDate.get(Calendar.MONTH) + 1));

			CQDAMDocument cqDoc = new CQDAMDocument(moNode, nepID);
			cqDoc.setAssetTitle(title);

			cqDoc.setAssetDescription(description);
			cqDoc.setLastModifiedBy(FeederConfig.cqUser);
			cqDoc.setAssetAuthor(creator);
			cqDoc.addMetadata("dc:date", pubDate);
			cqDoc.addMetadata("xmp:CreateDate", pubDate);
			cqDoc.addMetadata("dc:publisher", pubName);
			cqDoc.addMetadata("dc:subject", keywords + "," + location);
			cqDoc.addMetadata("dam:externalid", cnID);
			cqDoc.addMetadata("dc:identifier", nepID);
			cqDoc.addMetadata("dc:dccoverage", location);

			FileInputStream is = new FileInputStream(xmpFile);
			ValueFactory valueFactory = cqClient.getCqSession().getValueFactory();
			Binary contentValue = valueFactory.createBinary(is);
			cqDoc.setBinaryValue(contentValue, "image/jpg");

			cqClient.feedCQDoc(cqDoc);

			List<String> values = new ArrayList<String>();
			values.add(nepID);
			values.add(fullSlug);
			values.add(CNUtils.toSlug(creator));
			values.add(keywords);
			values.add(pubName);
			values.add(description);
			values.add(CNUtils.swapFirstLastName(creator, ','));
			values.add(title);
			values.add("");
			values.add("");
			values.add("");
			values.add("");
			values.add(cnID);
			values.add("");
			values.add(categories);
			values.add("162");

			if (counter % 100 == 0) {
				System.out.println("Pushed " + counter);
			}
			return values.toArray(new String[values.size()]);

		} catch (IOException e) {
			e.printStackTrace();
			logger.error(String.format("[%s] => [%s]", fileName, e.getMessage()));
		}

		return null;
	}
}
