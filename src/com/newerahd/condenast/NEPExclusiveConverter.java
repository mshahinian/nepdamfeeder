package com.newerahd.condenast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.ValueFactory;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import com.newerahd.config.FeederConfig;
import com.newerahd.dam.CQDAMClient;
import com.newerahd.dam.model.CQDAMDocument;

public class NEPExclusiveConverter {

	public static Logger logger = Logger.getLogger(NEPExclusiveConverter.class);
	public static String publicationPrefix = "New Era Portfolio";
	private static CSVFormat nepCSVFormat = CSVFormat
			.newBuilder(CSVFormat.EXCEL)
			.withQuoteChar('"')
			.withHeader("id", "product_id", "product_type", "artist_id", "image_code", "full_slug", "artist_slug", "orientation",
					"ratio", "products_name", "description", "artists_name", "long_bio", "short_bio", "colors", "color_family",
					"color_primary", "color_secondary", "categories", "categories_secondary", "keywords", "series_id",
					"thumbnail_width", "thumbnail_height", "collections", "score", "image_source", "release_date", "active",
					"updated").build();

	// formatter for mysql
	SimpleDateFormat mysqldf = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * Process New Yorker Data
	 * 
	 * @param fileName
	 *            - path to the csv file
	 * @param publicationSuffix
	 *            - publication name
	 * @param imageRootPath
	 *            - path to the images
	 * @return
	 * @throws Exception
	 */
	public static Map<String, Integer> handleNEP(String fileName, String publicationSuffix, String imageRootPath)
			throws Exception {
		CSVParser parser = null;
		int counter = 0;

		Map<String, Integer> errors = new HashMap<String, Integer>();

		CQDAMClient cqClient = new CQDAMClient(FeederConfig.cqServer, FeederConfig.cqUser, FeederConfig.cqPass,
				FeederConfig.cqDefaultWorkspace);
		Node source = cqClient.createFolder(FeederConfig.cqNEPRootFolder, publicationSuffix);
		Node rootFolder = cqClient.createFolder(source, "Exclusive");

		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
		try {
			parser = new CSVParser(in, nepCSVFormat);

			for (CSVRecord record : parser) {
				if (counter > 0) {
					String nepID = record.get("image_code");
					String title = record.get("products_name");
					String description = CNUtils.removeLineBreaks(record.get("description"));
					String keywords = CNUtils.removeLineBreaks(record.get("keywords"));
					String author = record.get("artists_name").trim();
					String authorProper = CNUtils.swapFirstLastName(author, ',');
					String categories = record.get("categories");

					Date releaseDate = null;
					Calendar releaseDateCal = Calendar.getInstance();
					try {
						releaseDate = DateUtils
								.parseDate(record.get("release_date"), new String[] { "yyyy-mm-dd", "mm/dd/yyyy" });
						releaseDateCal.setTime(releaseDate);
					} catch (java.text.ParseException pe) {
						System.out.println(pe.getMessage() + " " + nepID);
					}
					String colorFamily = "";
					String primaryColorHex = "";
					String secondaryColorHex = "";
					String orientation = "";

					/*
					 * Conde Nast concatenated descriptions and keywords in a
					 * single field - so we need to separate them and stick them
					 * in appropriate fields
					 */

					/*
					 * File assetFile = CNUtils.locateCNFile(imageRootPath,
					 * nepID); if (assetFile.getAbsoluteFile().exists()) {
					 * 
					 * ColorDetector nepCD = new ColorDetector(assetFile);
					 * orientation = nepCD.getImageOrientation(); List<String>
					 * allPrimaryHexColors = new
					 * ArrayList<String>(nepCD.getPrimaryHexColors());
					 * allPrimaryHexColors
					 * .addAll(nepCD.getColorFamilyHexColors());
					 * 
					 * primaryColorHex =
					 * StringUtils.join(allPrimaryHexColors,',').toUpperCase();
					 * secondaryColorHex =
					 * StringUtils.join(nepCD.getSecondaryHexColors
					 * (),',').toUpperCase(); colorFamily =
					 * StringUtils.join(nepCD.getColorFamilyNames(),',');
					 * logger.info(String.format("%s %s",
					 * assetFile.getAbsolutePath(), colorFamily )); }
					 */

					Node authorNode = cqClient.createFolder(rootFolder, authorProper);
					try {
						CQDAMDocument cqDoc = new CQDAMDocument(authorNode, nepID);
						cqDoc.setAssetTitle(title);

						cqDoc.setAssetDescription(description);
						cqDoc.setLastModifiedBy(FeederConfig.cqUser);
						cqDoc.setAssetAuthor(author);
						cqDoc.addMetadata("dc:date", releaseDateCal);
						cqDoc.addMetadata("dc:publisher", publicationPrefix + " " + publicationSuffix);
						cqDoc.addMetadata("dc:subject", keywords);
						cqDoc.addMetadata("dc:identifier", nepID);
						if (!StringUtils.isEmpty(categories)) {
							cqDoc.setAssetCQTags(categories.split(","));
						}

						/** Handle the binary file **/

						File assetFile = CNUtils.locateNEPFile(imageRootPath, nepID);

						//if (assetFile.exists()) {
							FileInputStream is = new FileInputStream(assetFile);
							ValueFactory valueFactory = cqClient.getCqSession().getValueFactory();
							Binary contentValue = valueFactory.createBinary(is);
							cqDoc.setBinaryValue(contentValue, "image/tiff");
						//}

						cqClient.feedCQDoc(cqDoc);
					} catch (Exception ex) {
						System.out.println("Unable to submit " + nepID + " " + ex.getMessage());
						ex.printStackTrace();
					}
					++counter;

					System.out.println("Processed " + nepID + " " + counter);

				}
				counter++;
			}
		} catch (IOException e) {
			logger.error("Problem occured " + e.getMessage());
			System.out.println(parser.getLineNumber() + " - " + parser.getRecordNumber());
			e.printStackTrace();
		} catch (Exception ex) {
			logger.error("Problem occured " + ex.getMessage());
			System.out.println(parser.getLineNumber() + " - " + parser.getRecordNumber());
			ex.printStackTrace();
		}

		// cqClient.close();
		return errors;
	}

}
