package com.newerahd.condenast.feeder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.jcr.RepositoryException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.newerahd.condenast.CNPublicationsXMLConverter;
import com.newerahd.condenast.NewYorkerConverter;
import com.newerahd.config.FeederConfig;
import com.newerahd.dam.CQDAMClient;

public class CNPublicationsCoverFeeder {
	public static Logger logger = Logger
			.getLogger(CNPublicationsCoverFeeder.class);
	CQDAMClient cqClient = null;
	CNPublicationsXMLConverter cqConverter = null;

	/**
	 * Default constructor
	 * 
	 * @throws RepositoryException
	 * @throws XPathExpressionException
	 * @throws ParserConfigurationException
	 */
	public CNPublicationsCoverFeeder() throws RepositoryException,
			XPathExpressionException, ParserConfigurationException {
		cqClient = new CQDAMClient(FeederConfig.cqServer, FeederConfig.cqUser,
				FeederConfig.cqPass, FeederConfig.cqDefaultWorkspace);
		cqConverter = new CNPublicationsXMLConverter();
	}

	public static void main(String[] args) {
		CommandLineParser cmdLineParser = new org.apache.commons.cli.BasicParser();
		Options options = new Options();
		Option help = new Option("help", "print this message");
		Option xmlpath = OptionBuilder.withArgName("xmlpath").hasArg()
				.withDescription("directory path for Conde Nast xml metadata")
				.create("xmlpath");
		Option imagepath = OptionBuilder.withArgName("imagepath").hasArg()
				.withDescription("root directory for Conde Nast Images")
				.create("imagepath");
		options.addOption(help);
		options.addOption(xmlpath);
		options.addOption(imagepath);

		try {
			CommandLine cmdLine = cmdLineParser.parse(options, args);
			String xmlFilePath = null;
			String baseImagePath = null;
		

			if (cmdLine.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("CNImageFeeder", options);
			}

			if (cmdLine.hasOption("xmlpath")) {
				xmlFilePath = (cmdLine.getOptionValue("xmlpath").toString());
			}
			
			if (cmdLine.hasOption("imagepath")) {
				baseImagePath = (cmdLine.getOptionValue("imagepath").toString());
			}
			CNPublicationsCoverFeeder pcf = new CNPublicationsCoverFeeder();
			pcf.processFolder(new File(xmlFilePath),baseImagePath);
			pcf.cqClient.close();
			
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		CNPublicationsCoverFeeder.logger.info("Finished");

	}

	public void processFolder(final File folder, String baseImagePath) throws Exception {

		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				processFolder(fileEntry, baseImagePath);
			} else {
				if (fileEntry.getName().toLowerCase().endsWith(".xml")) {
					logger.info(String.format("Processing %s",
							fileEntry.getAbsolutePath()));
					try {
					cqConverter.feedCNPublication(fileEntry.getAbsolutePath(), baseImagePath, cqClient);
					} catch (Exception ex) {
						logger.error(ex.getMessage());
					}
				}
			}
		}
	}
}
