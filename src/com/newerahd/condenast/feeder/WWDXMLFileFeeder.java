package com.newerahd.condenast.feeder;

import java.io.File;
import java.io.IOException;

import javax.jcr.RepositoryException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.csv.CSVPrinter;
import org.apache.log4j.Logger;

import com.newerahd.condenast.CNPublicationsXMLConverter;
import com.newerahd.condenast.CSVUtils;
import com.newerahd.condenast.XMPConverter;
import com.newerahd.config.FeederConfig;
import com.newerahd.dam.CQDAMClient;

/**
 * This feeder import womens worldwide daily images that have embedded xmp data
 * in them
 * 
 * @author mshahinian
 * 
 */
public class WWDXMLFileFeeder {

	public static Logger logger = Logger.getLogger(WWDXMLFileFeeder.class);
	CQDAMClient cqClient = null;
	XMPConverter xmpConverter = null;
	CSVPrinter csvPrinter = null;

	public WWDXMLFileFeeder() throws RepositoryException, XPathExpressionException, ParserConfigurationException, IOException {
		cqClient = new CQDAMClient(FeederConfig.cqServer, FeederConfig.cqUser, FeederConfig.cqPass,
				FeederConfig.cqDefaultWorkspace);
		xmpConverter = new XMPConverter();
		csvPrinter = CSVUtils.initCSVPrinter("xmpcsv.csv");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CommandLineParser cmdLineParser = new org.apache.commons.cli.BasicParser();
		Options options = new Options();
		Option help = new Option("help", "print this message");
		Option imagePathOption = OptionBuilder.withArgName("xmpimagepath").hasArg()
				.withDescription("directory path for Conde Nast images with xmp info embedded").create("xmpimagepath");
		options.addOption(help);
		options.addOption(imagePathOption);

		try {
			CommandLine cmdLine = cmdLineParser.parse(options, args);
			String imagePath = "";

			if (cmdLine.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("WWDXMLFileFeeder", options);
			}

			if (cmdLine.hasOption("xmpimagepath")) {
				imagePath = (cmdLine.getOptionValue("xmpimagepath").toString());
			}

			WWDXMLFileFeeder wf = new WWDXMLFileFeeder();
			wf.processFolder(new File(imagePath));
			wf.cqClient.close();
			wf.csvPrinter.flush();
			wf.csvPrinter.close();

		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		CNPublicationsCoverFeeder.logger.info("Finished");

	}

	public void processFolder(final File folder) throws Exception {

		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				processFolder(fileEntry);
			} else {
				if (fileEntry.getName().toLowerCase().endsWith(".jpg") || fileEntry.getName().toLowerCase().endsWith(".tif")) {
					System.out.println(String.format("Processing %s", fileEntry.getAbsolutePath()));
					// logger.info(String.format("Processing %s",
					// fileEntry.getAbsolutePath()));
					try {
						csvPrinter.printRecord(xmpConverter.feedCNPublication(fileEntry.getAbsolutePath(),"WWD", cqClient));
						
					} catch (Exception ex) {
						logger.error(ex.getMessage());
					}
				}
			}
		}
	}

}
