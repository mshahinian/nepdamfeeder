package com.newerahd.condenast.feeder;

import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.log4j.Logger;

import com.newerahd.condenast.NewYorkerConverter;

public class NewYorkerCartoonFeeder {
	public static Logger logger = Logger
			.getLogger(NewYorkerCartoonFeeder.class);

	public void feedTNYCSV(String fileName, String imagePath, String publication)
			throws Exception {

		// Map<String, Integer> errors =
		// NewYorkerConverter.handleNewYorker(fileName, "cartoons",
		// "W:\\Completed\\Publications\\NYCartoons\\Images");
		Map<String, Integer> errors = NewYorkerConverter.handleNewYorker(
				fileName, publication, imagePath);
		System.out.println("----------------------------");
		for (Map.Entry<String, Integer> kvp : errors.entrySet()) {
			System.out.println(String.format("%s => %d", kvp.getKey(),
					kvp.getValue()));
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CommandLineParser cmdLineParser = new org.apache.commons.cli.BasicParser();
		Options options = new Options();
		Option help = new Option("help", "print this message");
		Option csvfile = OptionBuilder.withArgName("csvfile").hasArg()
				.withDescription("csv file for New Yorker metadata")
				.create("csvfile");
		Option imagepath = OptionBuilder.withArgName("imagepath").hasArg()
				.withDescription("root directory for New Yorker Images")
				.create("imagepath");
		Option publication = OptionBuilder.withArgName("publication").hasArg()
				.withDescription("publication name: [Cartoons,Covers]")
				.create("publication");
		options.addOption(help);
		options.addOption(csvfile);
		options.addOption(imagepath);
		options.addOption(publication);

		try {
			CommandLine cmdLine = cmdLineParser.parse(options, args);
			String csvFilePath = null;
			String baseImagePath = null;
			String publicationName = null;

			if (cmdLine.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("NewYorkerFeeder", options);
			}

			if (cmdLine.hasOption("csvfile")) {
				csvFilePath = (cmdLine.getOptionValue("csvfile").toString());
			}
			
			if (cmdLine.hasOption("imagepath")) {
				baseImagePath = (cmdLine.getOptionValue("imagepath").toString());
			}
			
			if (cmdLine.hasOption("publication")) {
				publicationName = (cmdLine.getOptionValue("publication").toString());
			}
			
			NewYorkerCartoonFeeder nycf = new NewYorkerCartoonFeeder();

			nycf.feedTNYCSV(csvFilePath, baseImagePath, publicationName);

		} catch (org.apache.commons.cli.ParseException e) {
			e.printStackTrace();
		} catch (Exception ex) {
			NewYorkerCartoonFeeder.logger.error("Exception happened " + ex.getMessage());
		}

		NewYorkerCartoonFeeder.logger.info("Finished");

	}

}
