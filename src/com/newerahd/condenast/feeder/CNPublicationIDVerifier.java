package com.newerahd.condenast.feeder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.csv.CSVPrinter;
import org.apache.log4j.Logger;

import com.newerahd.condenast.CNPublicationsXMLConverter;
import com.newerahd.condenast.CSVUtils;

public class CNPublicationIDVerifier {

	public static Logger logger = Logger.getLogger(CNPublicationIDVerifier.class);
	CNPublicationsXMLConverter cqConverter = null;

	FileOutputStream fos;
	OutputStreamWriter csvWriter;
	CSVPrinter csvPrinter;
	public static String renameExampleHelp = "java  com.newerahd.condenast.feeder.CNPublicationIDVerifier -xmlpath C:\\temp\\cnfeeder\\CNDATA -imagepath W:\\Completed\\Publications\\All\\Images\\thumbnails\\600 -mode rename";

	/**
	 * Export Conde Nast all publications (the xml files not New Yorker spreadsheets) to nep CSV File
	 * 
	 * @param inputFolder
	 *            - input folder with all the xml files
	 * @param baseImagePath
	 *            - where all the images are located (not used in this case)
	 * @param publication
	 *            - Covers or cartoons
	 * @param outputFile
	 *            - where to write the outputcsv file
	 * @throws Exception
	 */
	private void exportCSV(String inputFolder, String baseImagePath, String outputFile) throws Exception {

		csvPrinter = CSVUtils.initCSVPrinter(outputFile);

		processFolder(new File(inputFolder), baseImagePath, false);

		csvPrinter.flush();
		csvPrinter.close();

	}

	public CNPublicationIDVerifier() throws XPathExpressionException, ParserConfigurationException {
		cqConverter = new CNPublicationsXMLConverter();
	}

	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		CommandLineParser cmdLineParser = new org.apache.commons.cli.BasicParser();
		Options options = new Options();
		Option help = new Option("help", "Example to rename:\n" + renameExampleHelp);
		Option xmlpath = OptionBuilder.withArgName("xmlpath").hasArg().withDescription("Metadata dir for CN xml")
				.create("xmlpath");
		Option imagepath = OptionBuilder.withArgName("imagepath").hasArg().withDescription("Root directory with CN Images")
				.create("imagepath");
		Option mode = OptionBuilder.withArgName("mode").hasArg().withDescription("Mode: rename, csvexport").create("mode");

		Option nepcsvfile = OptionBuilder.withArgName("nepcsvfile").hasArg()
				.withDescription("NEP csv file to export Example: c:\\myfiles.csv").create("nepcsvfile");

		options.addOption(mode);
		options.addOption(help);
		options.addOption(xmlpath);
		options.addOption(imagepath);
		options.addOption(nepcsvfile);

		try {
			CommandLine cmdLine = cmdLineParser.parse(options, args);
			String xmlFilePath = null;
			String baseImagePath = null;
			String outputFilePath = null;
			Boolean rename = false;

			if (cmdLine.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("CNPublicationVerifier", options);
			}

			if (cmdLine.hasOption("xmlpath")) {
				xmlFilePath = (cmdLine.getOptionValue("xmlpath").toString());
			}

			if (cmdLine.hasOption("imagepath")) {
				baseImagePath = (cmdLine.getOptionValue("imagepath").toString());
			}

			if (cmdLine.hasOption("nepcsvfile")) {
				outputFilePath = (cmdLine.getOptionValue("nepcsvfile").toString());
			}

			if (cmdLine.hasOption("mode")) {
				rename = (cmdLine.getOptionValue("mode").toString().toLowerCase().equals("rename"));
			}

			CNPublicationIDVerifier pcf = new CNPublicationIDVerifier();
			if (rename) {
				logger.info("Renaming files in " + baseImagePath);
				pcf.processFolder(new File(xmlFilePath), baseImagePath, rename);
			} else {
				logger.info("Exporting to csv to:" + outputFilePath +  " from xml dir" + xmlFilePath);
				pcf.exportCSV(xmlFilePath, baseImagePath, outputFilePath);
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			ex.printStackTrace();
		}
		CNPublicationIDVerifier.logger.info("Finished");
	}

	public void processFolder(final File folder, String baseImagePath, boolean rename) throws Exception {
		switch("fffff") {
		case "lalala":
		
		default:
			break;
		}
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				processFolder(fileEntry, baseImagePath, rename);
			} else {
				if (fileEntry.getName().toLowerCase().endsWith(".xml")) {
					// logger.info(String.format("Processing %s",fileEntry.getAbsolutePath()));
					if (rename) {
						cqConverter.renameCNFile(fileEntry.getAbsolutePath(), baseImagePath);
					} else {
						try {
							csvPrinter.printRecord(cqConverter.cnXMLtoCSV(fileEntry.getAbsolutePath(), baseImagePath));
						} catch (Exception ex) {
							logger.error(ex.getMessage());
							ex.printStackTrace();
						}
					}
				}
			}
		}
	}
}
