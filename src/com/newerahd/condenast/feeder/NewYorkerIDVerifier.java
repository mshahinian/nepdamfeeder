package com.newerahd.condenast.feeder;

import java.io.FileReader;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.newerahd.condenast.CSVUtils;
import com.newerahd.condenast.NewYorkerConverter;

public class NewYorkerIDVerifier {
	public static Logger logger = Logger.getLogger(NewYorkerIDVerifier.class);
	NewYorkerConverter nyc = new NewYorkerConverter();

	private void exportCSV(String inputFile, String publication, String baseImagePath, String outputFile) throws Exception {

		CSVParser parser = new CSVParser(new FileReader(inputFile), CSVFormat.DEFAULT);
		CSVPrinter csvPrinter = CSVUtils.initCSVPrinter(outputFile);
		for (CSVRecord record : parser) {
			if (record.getRecordNumber() > 1) {
				csvPrinter.printRecord((Object[]) nyc.cnCSVTOnepCSV(record, publication, baseImagePath));
			}
		}
		csvPrinter.flush();
		csvPrinter.close();

	}

	/**
	 * Rename the images supplied by Conde Nast from Conde Nast ID to NEP ID
	 * 
	 * @param inputFile
	 *            - csv File with metadata
	 * @param baseImagePath
	 *            - directory with base images
	 * @param publication
	 *            - name of the new yorker publication
	 * @throws Exception
	 */
	private void renameFiles(String inputFile, String baseImagePath, String publicationName) throws Exception {
		CSVParser parser = new CSVParser(new FileReader(inputFile), CSVFormat.EXCEL);
		for (CSVRecord record : parser) {
			if (record.getRecordNumber() > 1) {
				nyc.renameImageFile(record, baseImagePath, publicationName);
			}
		}
	}

	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		CommandLineParser cmdLineParser = new org.apache.commons.cli.BasicParser();
		Options options = new Options();
		Option help = new Option("help", "print this message");
		Option csvfile = OptionBuilder.withArgName("csvfile").hasArg().withDescription("csv file for New Yorker metadata")
				.create("csvfile");
		Option nepcsvfile = OptionBuilder.withArgName("nepcsvfile").hasArg()
				.withDescription("NEP csv file to export Example: c:\\myfiles.csv").create("nepcsvfile");
		Option imagepath = OptionBuilder.withArgName("imagepath").hasArg()
				.withDescription("root directory for New Yorker Images").create("imagepath");
		Option publication = OptionBuilder.withArgName("publication").hasArg()
				.withDescription("publication name: [Cartoons,Covers]").create("publication");
		Option mode = OptionBuilder.withArgName("mode").hasArg().withDescription("Mode: rename, csvexport").create("mode");

		options.addOption(help);
		options.addOption(mode);
		options.addOption(csvfile);
		options.addOption(imagepath);
		options.addOption(publication);
		options.addOption(nepcsvfile);

		try {
			CommandLine cmdLine = cmdLineParser.parse(options, args);
			String csvFilePath = null;
			String baseImagePath = null;
			String publicationName = null;
			String outputFilePath = null;
			Boolean rename = false;

			if (cmdLine.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("NewYorkerFeeder", options);
			}

			if (cmdLine.hasOption("csvfile")) {
				csvFilePath = (cmdLine.getOptionValue("csvfile").toString());
			}

			if (cmdLine.hasOption("imagepath")) {
				baseImagePath = (cmdLine.getOptionValue("imagepath").toString());
			}

			if (cmdLine.hasOption("publication")) {
				publicationName = (cmdLine.getOptionValue("publication").toString());
			}

			if (cmdLine.hasOption("mode")) {
				rename = (cmdLine.getOptionValue("mode").toString().toLowerCase().equals("rename"));
			}

			if (cmdLine.hasOption("nepcsvfile")) {
				outputFilePath = (cmdLine.getOptionValue("nepcsvfile").toString());
			}

			if (!rename && StringUtils.isEmpty(outputFilePath)) {
				throw new Exception("Please specify the output csv file with -nepcsvfile");
			}

			NewYorkerIDVerifier nyidv = new NewYorkerIDVerifier();
			/** We can export as CSV or rename files **/
			if (rename) {
				nyidv.renameFiles(csvFilePath, baseImagePath, publicationName);
			} else {
				logger.info("Exporting CN CSV to NEP CSV");
				nyidv.exportCSV(csvFilePath, publicationName, baseImagePath, outputFilePath);

			}
		} catch (org.apache.commons.cli.ParseException e) {
			e.printStackTrace();
		} catch (Exception ex) {
			NewYorkerCartoonFeeder.logger.error("Exception happened " + ex.getMessage());
			ex.printStackTrace();
		}

		NewYorkerCartoonFeeder.logger.info("Finished");

	}
}
