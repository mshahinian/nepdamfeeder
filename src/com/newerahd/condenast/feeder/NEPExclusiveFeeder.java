package com.newerahd.condenast.feeder;

import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.log4j.Logger;

import com.newerahd.condenast.NEPExclusiveConverter;


public class NEPExclusiveFeeder {
	public static Logger logger = Logger
			.getLogger(NEPExclusiveFeeder.class);

	public void feedNEPCSV(String fileName, String imagePath, String publication)
			throws Exception {

		Map<String, Integer> errors = NEPExclusiveConverter.handleNEP(
				fileName, publication, imagePath);
		System.out.println("----------------------------");
		for (Map.Entry<String, Integer> kvp : errors.entrySet()) {
			System.out.println(String.format("%s => %d", kvp.getKey(),
					kvp.getValue()));
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CommandLineParser cmdLineParser = new org.apache.commons.cli.BasicParser();
		Options options = new Options();
		Option help = new Option("help", "print this message");
		Option csvfile = OptionBuilder.withArgName("csvfile").hasArg()
				.withDescription("csv file for NEP exclusive metadata")
				.create("csvfile");
		Option imagepath = OptionBuilder.withArgName("imagepath").hasArg()
				.withDescription("root directory for NEP eclusive Images")
				.create("imagepath");
		Option publication = OptionBuilder.withArgName("publication").hasArg()
				.withDescription("publication name: [NEP,Covers]")
				.create("publication");
		options.addOption(help);
		options.addOption(csvfile);
		options.addOption(imagepath);
		options.addOption(publication);

		try {
			CommandLine cmdLine = cmdLineParser.parse(options, args);
			String csvFilePath = null;
			String baseImagePath = null;
			String publicationName = null;

			if (cmdLine.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("NEPExclusiveFeeder", options);
			}

			if (cmdLine.hasOption("csvfile")) {
				csvFilePath = (cmdLine.getOptionValue("csvfile").toString());
			}
			
			if (cmdLine.hasOption("imagepath")) {
				baseImagePath = (cmdLine.getOptionValue("imagepath").toString());
			}
			
			if (cmdLine.hasOption("publication")) {
				publicationName = (cmdLine.getOptionValue("publication").toString());
			}
			
			NEPExclusiveFeeder nycf = new NEPExclusiveFeeder();

			nycf.feedNEPCSV(csvFilePath, baseImagePath, "New Era Portfolio");

		} catch (org.apache.commons.cli.ParseException e) {
			e.printStackTrace();
		} catch (Exception ex) {
			NEPExclusiveFeeder.logger.error("Exception happened " + ex.getMessage());
			ex.printStackTrace();
		}

		NEPExclusiveFeeder.logger.info("Finished");

	}
}
