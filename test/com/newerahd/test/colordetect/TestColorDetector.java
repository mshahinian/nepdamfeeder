package com.newerahd.test.colordetect;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.newerahd.colordetect.ColorDetector;
import com.newerahd.colordetect.ColorNamesDictionary;
import com.newerahd.colordetect.NEPColorModel;

public class TestColorDetector {

	public static String[] files = { "c:\\temp\\CN109032MM.jpg", "c:\\temp\\CN110277GM.jpg", "c:\\temp\\CN65601WHG.jpg",
			"c:\\temp\\CN66696WGL.jpg", "c:\\temp\\TC112A.jpg", "c:\\temp\\CN46394WNC.jpg", "c:\\temp\\CN46877WNC.jpg",
			"c:\\temp\\CN46891WNC.jpg", "c:\\temp\\CN46936WNC.jpg" };

	public static String[] files1 = { "c:\\temp\\200\\CN00007903.jpg", "c:\\temp\\200\\CN00058953.jpg",
			"c:\\temp\\200\\CN00059047.jpg", "c:\\temp\\200\\CN00059048.jpg", "c:\\temp\\200\\CN00159472.jpg",
			"c:\\temp\\200\\CN00159725.jpg", "c:\\temp\\200\\CN46402WNC.jpg", "c:\\temp\\200\\CN46536WNC.jpg",
			"c:\\temp\\200\\CN46562WNC.jpg", "c:\\temp\\200\\CN46563WNC.jpg", "c:\\temp\\200\\CN46564WNC.jpg",
			"c:\\temp\\200\\CN46728WNC.jpg", "c:\\temp\\200\\CN46729WNC.jpg", "c:\\temp\\200\\CN46730WNC.jpg",
			"c:\\temp\\200\\CN46741WNC.jpg", "c:\\temp\\200\\CN46742WNC.jpg", "c:\\temp\\200\\CN46744WNC.jpg",
			"c:\\temp\\200\\CN46768WNC.jpg", "c:\\temp\\200\\CN46775WNC.jpg", "c:\\temp\\200\\CN46778WNC.jpg",
			"c:\\temp\\200\\CN46779WNC.jpg", "c:\\temp\\200\\CN46780WNC.jpg", "c:\\temp\\200\\CN46782WNC.jpg",
			"c:\\temp\\200\\CN46784WNC.jpg", "c:\\temp\\200\\CN46822WNC.jpg", "c:\\temp\\200\\CN46825WNC.jpg",
			"c:\\temp\\200\\CN46827WNC.jpg", "c:\\temp\\200\\CN46828WNC.jpg", "c:\\temp\\200\\CN46829WNC.jpg",
			"c:\\temp\\200\\CN46830WNC.jpg", "c:\\temp\\200\\CN46831WNC.jpg", "c:\\temp\\200\\CN46832WNC.jpg",
			"c:\\temp\\200\\CN46834WNC.jpg", "c:\\temp\\200\\CN46838WNC.jpg", "c:\\temp\\200\\CN46839WNC.jpg",
			"c:\\temp\\200\\CN46845WNC.jpg", "c:\\temp\\200\\CN46846WNC.jpg", "c:\\temp\\200\\CN46850WNC.jpg",
			"c:\\temp\\200\\CN46851WNC.jpg", "c:\\temp\\200\\CN46854WNC.jpg", "c:\\temp\\200\\CN46877WNC.jpg",
			"c:\\temp\\200\\CN46878WNC.jpg", "c:\\temp\\200\\CN46879WNC.jpg", "c:\\temp\\200\\CN46888WNC.jpg",
			"c:\\temp\\200\\CN46891WNC.jpg", "c:\\temp\\200\\CN46892WNC.jpg", "c:\\temp\\200\\CN49749WNC.jpg",
			"c:\\temp\\200\\CN49763WNC.jpg", "c:\\temp\\200\\CN49829WNC.jpg", "c:\\temp\\200\\CN49879WNC.jpg" };

	public static String[] files2 = { "c:\\temp\\200\\CN46851WNC.jpg", "c:\\temp\\200\\CN46854WNC.jpg", "c:\\temp\\200\\CN46877WNC.jpg","c:\\temp\\200\\CN46784WNC.jpg", "c:\\temp\\200\\CN46822WNC.jpg","c:\\temp\\200\\CN49763WNC.jpg", "c:\\temp\\200\\CN49829WNC.jpg" };
	public static void main(String args[]) throws Exception {

		
		List<String> content = new ArrayList<String>();
		List<String> fullSpectrum = new ArrayList<String>();
		for (String s : NEPColorModel.colorSpectrum) {
			fullSpectrum.add(s);
		}
//		content.add(deleteMe(fullSpectrum));
		content.add("<table>");
		String[] filesToIterate = files1;
		for (int i = 0; i < filesToIterate.length; i++) {
			File f = new File(filesToIterate[i]);
			try {
				System.out.println(f.getAbsolutePath());
				ColorDetector nepCD = new ColorDetector(f);

				content.add("<tr>");
				content.add("<td valign='top'><img src='" + f.getAbsolutePath() + "' style='width:200px;'/><br/>"	+ f.getAbsolutePath() + "<br/>" + nepCD.getImageOrientation() + "</td>");
				content.add("<td valign='top'>");
				content.add("<br/>Primary Colors: " + deleteMe(nepCD.getPrimaryHexColors()));
				content.add("<br/>Secondary Colors: " + deleteMe(nepCD.getSecondaryHexColors()));
				content.add("<br/>Color Family: " + deleteMe(nepCD.getColorFamilyHexColors()));
				
				
				content.add("</td>");
				content.add("<tr>");
				
				/*
				 * StringBuilder so = new StringBuilder();
		//so.append(printReducedListContents(list2));
		//so.append("<br/><hr><br/>");
		so.append(printReducedListContents(list3));
		so.append("<br/>Primary Colors: " + deleteMe(nepcm.getPrimaryHexColors()));
		so.append("<br/>Secondary Colors: " + deleteMe(nepcm.getSecondaryHexColors()));
		so.append("<br/>Color Family: " + deleteMe(nepcm.getColorFamilyHexColors()));
		
		so.append(String.format("Original size %d Reduced size %d Reduced2 size %d Reduced3 size %d", m.size(), reduced1.size(), reduced2.size(), reduced3.size()));
		
				 */
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		content.add("</table>");
		writeText("c:\\temp\\colortest.html", content);

	}

	public static void writeText(String aFileName, List<String> aLines) throws IOException {
		Path path = Paths.get(aFileName);
		try (BufferedWriter writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8)) {
			for (String line : aLines) {
				writer.write(line);
				writer.newLine();
			}
		}
	}

	public static String deleteMe(List<String> list) {

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < list.size(); i++) {
			sb.append(String.format("<div style='background-color:%s; width:200px;'>#%s %s</div>", list.get(i), list.get(i),
					ColorNamesDictionary.getColorName(list.get(i))));
		}
		return sb.toString();
	}

	public static String printReducedListContents(List<Map.Entry<Integer, int[]>> list) {
		return printReducedListContents(list, 10);
	}

	public static String printReducedListContents(List<Map.Entry<Integer, int[]>> list, int maxEntries) {

		StringBuilder sb = new StringBuilder();
		int maxSize = (list.size() < maxEntries) ? list.size() : maxEntries;
		int[] previousRGB = null;
		double distance = 0;
		for (int i = 0; i < maxSize; i++) {
			Map.Entry<Integer, int[]> e = (Map.Entry<Integer, int[]>) list.get(i);
			int width = (int) Math.floor(e.getValue()[1]) + 120;
			int[] currentColor = ColorDetector.getRGBArr(e.getKey());
			String hexColor = ColorDetector.rgbToHexString(currentColor);
			if (previousRGB != null) {
				distance = ColorDetector.colorDistance(currentColor, previousRGB);
			}
			previousRGB = currentColor;
			sb.append(String.format("<div style='background-color:%s; width:%dpx;'>%s - d %.1f</div>", hexColor, width, hexColor,
					distance));
		}
		return sb.toString();
	}

}
